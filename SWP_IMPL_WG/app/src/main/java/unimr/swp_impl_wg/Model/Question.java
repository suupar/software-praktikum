package unimr.swp_impl_wg.Model;


/**
 * @author Fabio Geiger, Tim Waldhans
 * This Class represents one Question object with its answers and Information
 */

public class Question {

    /**
     * @param question    Question to be answered
     * @param answ        Array of Three Wrong answers
     * @param answ_true   One (true) Answer
     * @param givenAnswer int for given answer
     */

    private String question;
    private String answ[];
    private String answ_true;
    private String givenAnswer;

    /**
     * This method returns the Answers as a String Array
     * @return Answers represented as a String Array
     */

    public String[] getAnsw() {
        return answ;
    }


    /**
     * @param question    Question to be answered passed to constructor
     * @param answ        Array of Three Wrong answers
     * @param answ_true   One (true) Answer       passed to constructor
     * @param givenAnswer int for given answer  passed to constructor
     */

    public Question(String question, String[] answ, String answ_true, String givenAnswer) {
        this.question = question;
        this.answ = answ;
        this.answ_true = answ_true;
        this.givenAnswer = givenAnswer;
    }

    /**
     * Empty  dummy Constructor
     */

    public Question() {
        super();

    }

    /**
     * Getters and Setters
     */

    public String getQuestion() {
        return question;
    }


    public String getAnsw_true() {
        return answ_true;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getGivenAnswer() {
        return givenAnswer;
    }

    public void setGivenAnswer(String givenAnswer) {
        this.givenAnswer = givenAnswer;
    }

    public boolean answeredCorrect() {
        return answ_true.equals(givenAnswer);
    }


    /**
     * This method checks an answer has been answered
     * @return answered?
     */
    public boolean answered() {
        if (givenAnswer.equals("")) {
            return false;
        } else {
            return true;
        }

    }

}



package unimr.swp_impl_wg.Model;

/**
 * @author Tim Waldhans, Fabio Geiger
 */
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import unimr.swp_impl_wg.Model.Question;
import unimr.swp_impl_wg.Model.QuestionData;
import unimr.swp_impl_wg.R;

/**
 * This class builds an ArrayAdapter for the ResultViewActivity Activity
 */

public class QuestionArrayAdapter extends ArrayAdapter<Question> {

    /**
     * @param context   Passed Context
     * @param questions Question Array
     * @param cat       Category var
     * @param red       Color final
     * @param green     Color final
     */
    private final Context context;
    private final Question[] questions;
    private final int cat;
    final private int red = Color.RED;
    final private int green = Color.GREEN;

    /**
     * Constructor
     * @param context   Passed Context
     * @param questions Passed Questions
     * @param cat       Passed category
     */


    public QuestionArrayAdapter(Context context, Question[] questions, int cat) {
        super(context, -1, questions);
        this.context = context;
        this.questions = questions;
        this.cat = cat;
    }

    /**
     *
     * @param position     Line position for Data from the adapter
     * @param convertView  Passed View to adapt
     * @param parent       Passed parent View
     * @return             Constructed Row View
     */

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.rowlayout, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.label);
        if (questions[position].getGivenAnswer().equals(questions[position].getAnsw_true())) {
            textView.setBackgroundColor(green);
            textView.setText("Kategorie: " + QuestionData.getInstance().getCategories().get(cat).getCategoryName() + "; Frage: \"" + questions[position].getQuestion() + "\"; " +
                    "\ngegebene Antwort: \"" + questions[position].getGivenAnswer()+ "\" ") ;
        } else {
            textView.setBackgroundColor(red);
            textView.setText("Kategorie: " + QuestionData.getInstance().getCategories().get(cat).getCategoryName() + "; Frage: \"" + questions[position].getQuestion() + "\"; " +
                    "\ngegebene Antwort: \"" + questions[position].getGivenAnswer() + "\" --> richtige Antwort: \"" + questions[position].getAnsw_true()+"\"");
        }
        return rowView;
    }
}

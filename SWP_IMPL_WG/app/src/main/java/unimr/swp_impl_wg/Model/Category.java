package unimr.swp_impl_wg.Model;

/**
 * @author Tim Waldhans, Fabio Geiger
 */

import java.util.LinkedList;
import java.util.List;

/**
 * This class represents a question Category
 */

public class Category {

    /**
     * @param categoryName Category Name
     * @param level1 Category List for level 1
     * @param level1 Category List for level 2
     * @param level1 Category List for level 3
     * @see LinkedList
     */

    private String categoryName;
    public List<Question> level1 = new LinkedList<>();
    public List<Question> level2 = new LinkedList<>();
    public List<Question> level3 = new LinkedList<>();

    /**
     * Category constructor
     * @param CategoryName Passed Category name
     */

    public Category(String CategoryName) {
        this.categoryName = CategoryName;
    }

    /**
     * Getter for category
     * @return Category name
     */
    public String getCategoryName() {
        return categoryName;
    }

    public Category(){
        super();
    }

}

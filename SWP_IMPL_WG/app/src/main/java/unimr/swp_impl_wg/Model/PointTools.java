package unimr.swp_impl_wg.Model;

/**
 * @author Fabio Geiger, Tim Waldhans
 */

import android.content.SharedPreferences;

/**
 * This Class Handles the Gaming Points
 */

public class PointTools {

    /**
     * This methods sets the Points in the global SharedPrefrences
     * @param storage Storage to transfer to
     * @param points  Points to commit
     * @param prefs   SharedPreferences reference
     * @return Success ?: true , false
     * This method saves points into chosen Storage Type
     * @see SharedPreferences
     */
    public static boolean setPoints(int storage, int points, SharedPreferences prefs) {

        switch (storage) {
            case 0:
                return prefs.edit().putInt("InitialPoints", points).commit();
            case 1:
                return prefs.edit().putInt("SafeActivity", points).commit();
            case 2:
                return prefs.edit().putInt("WonPoints", points).commit();
        }

        return false;
    }

    /**
     * This Method returns Points from picked storage
     * @param storage Storage to get Points from
     * @param prefs   SharedPreferences reference
     * @return points to give back
     */

    public static int getPoints(int storage, SharedPreferences prefs) {
        switch (storage) {
            case 0:
                return  prefs.getInt("InitialPoints", 0);

            case 1:
                return  prefs.getInt("SafeActivity", 0);

            case 2:
                return  prefs.getInt("WonPoints", 0);

        }
        return 0;
    }

    /**
     * Returns all available Points
     * @param prefs  Passed preferences
     * @return Availiable Points
     */
    public static int getAllPlayingPoints(SharedPreferences prefs) {
        int p = 0;
        return p = prefs.getInt("InitialPoints", 0) + prefs.getInt("WonPoints", 0);
    }

    /**
     * Sets lost points
     * @param prefs Passed preferences
     * @param loss  Points to substract
     */

    public static void lostPoints(SharedPreferences prefs, int loss) {
        int init = prefs.getInt("InitialPoints", 0), won = prefs.getInt("WonPoints", 0);
        if (loss > init) {
            loss = loss - init;
            init = 0;
            won = won - loss;
        } else {
            init = init - loss;
        }
        PointTools.setPoints(0, init, prefs);
        PointTools.setPoints(2, won, prefs);
    }


}

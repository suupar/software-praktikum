package unimr.swp_impl_wg.Model;

import unimr.swp_impl_wg.sensor.BreathSensor;
import unimr.swp_impl_wg.sensor.BreathSensorDataProcessor;

/**
 * This class Handles the Sensor Data
 *
 * @author Tim Waldhans, Fabio Geiger
 */

public class SensorData {

    /**
     * @param instance Current Sensor Data Instance
     * @param sensor   Data Sensor
     * @param sdp      BreathSensor Data Processor
     */

    private static SensorData instance;
    private BreathSensor sensor;
    private BreathSensorDataProcessor sdp;


    /**
     * Private SConstructor
     */
    private SensorData() {
    }

    /**
     * Creates Singleton, returns instance
     *
     * @return Instance of this Singleton
     */

    public static SensorData getInstance() {
        if (instance == null) {
            instance = new SensorData();
        }
        return instance;
    }

    /**
     * Getter for Sensor
     *
     * @return Sensor Object
     */

    public BreathSensor getSensor() {
        return sensor;
    }

    /**
     * Sensor Setter
     *
     * @param bs Breath Sensor to set
     */

    public void setSensor(BreathSensor bs) {
        this.sensor = bs;
    }

    /**
     * Returns BreathSensorDataProcessor
     *
     * @return BreathSensorDataProcessor for Singleton
     */

    public BreathSensorDataProcessor getBp() {
        return sdp;
    }

    /**
     * Set BreathSensorDataProcessor
     *
     * @param bp BreathDataSensorProcessor to set
     */

    public void setBp(BreathSensorDataProcessor bp) {
        this.sdp = bp;
    }
}

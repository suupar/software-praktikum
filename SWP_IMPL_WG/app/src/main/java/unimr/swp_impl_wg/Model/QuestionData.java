package unimr.swp_impl_wg.Model;


import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * This class is a static Question Data Handler
 *
 * @author Tim Waldhans, Fabio Geiger
 */

public class QuestionData {

    /**
     * @param QuestionData singleton-needed Class instance
     * @param numberofQuestions Questions per Answering Sequence
     * @param pickedQuestions   Already answered Questions
     * @param Categories List of QuestionData
     */
    private static QuestionData instance;


    private int numberofQuestions = 0;
    private Question[] pickedQuestions;
    private List<Category> Categories = new LinkedList<>();

    /**
     * Returns Number of Questions in this Handler
     *
     * @return Number of Questions
     */

    public int getNumberofQuestions() {
        return numberofQuestions;
    }

    public void setNumberofQuestions(int numberofQuestions) {
        this.numberofQuestions = numberofQuestions;
    }


    public static QuestionData getInstance() {
        if (instance == null) {
            instance = new QuestionData();
        }
        return instance;
    }


    public QuestionData() {
        super();
    }


    public Question[] getPickedQuestions() {
        return pickedQuestions;
    }

    public void setPickedQuestions(Question[] pickedQuestions) {
        this.pickedQuestions = pickedQuestions;
    }


    public List<Category> getCategories() {
        return Categories;
    }

    public void setCategories(List<Category> categories) {
        this.Categories = categories;
    }


    /**
     * Empties Category List
     */
    public void empty() {
        setCategories(new LinkedList<Category>());
        numberofQuestions = 0;
    }

    public int readGameDataFromJson(Context c) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        // Convert JSON string from file to Object
        try {
            File testfile = new File(c.getFilesDir().getAbsoluteFile(), "questions.json");
            instance = mapper.readValue(testfile, QuestionData.class);
        } catch (JsonGenerationException e) {
            Log.e("writeJson", "Message is: " + e.getMessage());
        } catch (JsonMappingException e) {
            Log.e("writeJson", "Message is: " + e.getMessage());
        } catch (IOException e) {
            Log.e("writeJson", "Message is: " + e.getMessage());
        }
        int noq = 0;
        for (int i = 0; i < instance.Categories.size(); i++) {
            for (int j = 0; j < instance.Categories.get(i).level1.size(); j++) {
                Question q = instance.Categories.get(i).level1.get(j);
                if (!q.answered()) {
                    noq++;
                }
            }
            for (int j = 0; j < instance.Categories.get(i).level2.size(); j++) {
                Question q = instance.Categories.get(i).level2.get(j);
                if (!q.answered()) {
                    noq++;
                }
            }
            for (int j = 0; j < instance.Categories.get(i).level3.size(); j++) {
                Question q = instance.Categories.get(i).level3.get(j);
                if (!q.answered()) {
                    noq++;
                }
            }
        }
        return noq;
    }


    public void writeGameDataToJson(Context c) {
        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter write = mapper.writerWithDefaultPrettyPrinter();
        try {
            write.writeValue(new File(c.getFilesDir().getAbsoluteFile(), "questions.json"), instance);
        } catch (JsonGenerationException e) {
            Log.e("writeJson", "Message is: " + e.getMessage());
        } catch (JsonMappingException e) {
            Log.e("writeJson", "Message is: " + e.getMessage());
        } catch (IOException e) {
            Log.e("writeJson", "Message is: " + e.getMessage());
        }
    }


    public void updateJSONFile(Question[] picked, int cat, int diff, Context c) {
        int index;
        for (Question aPicked : picked) {
            switch (diff) {
                case 1:
                    index = QuestionData.getInstance().getCategories().get(cat).level1.indexOf(aPicked);
                    QuestionData.getInstance().getCategories().get(cat).level1.get(index).setGivenAnswer(aPicked.getGivenAnswer());
                    break;
                case 2:
                    index = QuestionData.getInstance().getCategories().get(cat).level2.indexOf(aPicked);
                    QuestionData.getInstance().getCategories().get(cat).level2.get(index).setGivenAnswer(aPicked.getGivenAnswer());
                    break;
                case 3:
                    index = QuestionData.getInstance().getCategories().get(cat).level3.indexOf(aPicked);
                    QuestionData.getInstance().getCategories().get(cat).level3.get(index).setGivenAnswer(aPicked.getGivenAnswer());
                    break;
            }
        }
        writeGameDataToJson(c);
    }

    public static Question[] pickQuestions(int cat, int diff, SharedPreferences prefs) {

        Question[] out = new Question[prefs.getInt("QuestionsPerRound", 10)];
        int outcount = 0;
        switch (diff) {
            case 1:
                for (int i = 0; i < QuestionData.getInstance().getCategories().get(cat).level1.size() && outcount < out.length; i++) {
                    if (!QuestionData.getInstance().getCategories().get(cat).level1.get(i).answered()) {
                        out[outcount] = QuestionData.getInstance().getCategories().get(cat).level1.get(i);
                        outcount++;
                    }

                }
                break;
            case 2:
                for (int i = 0; i < QuestionData.getInstance().getCategories().get(cat).level2.size() && outcount < out.length; i++) {
                    if (!QuestionData.getInstance().getCategories().get(cat).level2.get(i).answered()) {
                        out[outcount] = QuestionData.getInstance().getCategories().get(cat).level2.get(i);
                        outcount++;
                    }
                }
                break;
            case 3:
                for (int i = 0; i < QuestionData.getInstance().getCategories().get(cat).level3.size() && outcount < out.length; i++) {
                    if (!QuestionData.getInstance().getCategories().get(cat).level3.get(i).answered()) {
                        out[outcount] = QuestionData.getInstance().getCategories().get(cat).level3.get(i);
                        outcount++;
                    }
                }
                break;

        }
        return out;
    }

    public static int getUnanswered(SharedPreferences prefs) {
        return prefs.getInt("unansweredQuestions", 0);
    }

    public static boolean setUnanswered(SharedPreferences prefs, int count) {
        return prefs.edit().putInt("unansweredQuestions", count).commit();
    }
}

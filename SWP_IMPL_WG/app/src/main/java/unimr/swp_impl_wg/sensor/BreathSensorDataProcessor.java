package unimr.swp_impl_wg.sensor;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import android.util.Log;

/**
 * @author Felix Rieger
 */
public class BreathSensorDataProcessor extends Observable {
    private List<Double> values = new LinkedList<Double>();
    private final int ds;
    private final static double MINCHANGE = 25;
    private double dcOffset = 0;
    private boolean dynamicDcOffset = false;

    /**
     * Constructor
     *
     * @param b        Breath sensor
     * @param dataSize number of measurements to keep. Higher values enable better detection of events, but will cause noticeable lag due to detection of past events
     */
    public BreathSensorDataProcessor(BreathSensor b, int dataSize) {
        ds = dataSize;
        b.addObserver(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                if (values.size() > ds) {
                    values.remove(0);
                }
                values.add((Double) data);
                setChanged();
                notifyObservers();
            }
        });
    }

    /**
     * Get the last raw value
     *
     * @return
     */
    public double getLastValue() {
        return values.get(values.size() - 1);
    }

    /**
     * Compute the DC offset from the list of measurements
     *
     * @return average of the measurements
     */
    public double computeDcOffset() {
        double acc = 0;
        for (double d : values) {
            acc += d;
        }
        return acc / values.size();
    }

    /**
     * Set the DC offset
     *
     * @param offset
     */
    public void setDcOffset(double offset) {
        dcOffset = offset;
    }

    /**
     * Enable dynamic DC offset computation
     *
     * @param enable
     */
    public void enableDynamicDcOffset(boolean enable) {
        dynamicDcOffset = enable;
    }

    /**
     * (compute and) get the current DC offset
     * Computes the DC offset before returning it if dynamic DC offset is enabled.
     *
     * @return current DC offset
     */
    private double getDcOffset() {
        if (dynamicDcOffset) {
            setDcOffset(computeDcOffset());
        }
        return dcOffset;
    }

    /**
     * Remove an offset from a list of measurements
     *
     * @param orig   measurements
     * @param offset offset
     * @return list with offset removed
     */
    private List<Double> removeOffset(List<Double> orig, double offset) {
        List<Double> ret = new LinkedList<Double>();
        for (double d : orig) {
            ret.add(d - offset);
        }
        return ret;
    }

    /**
     * Detects breath mode A1 "ausatmen und einatmen"
     *
     * @param v         list of measurements
     * @param minChange minimum change to react to (noise filtering)
     * @return true if mode detected, false otherwise
     */
    private boolean detectBreathModeA1(List<Double> v, double minChange) {
        // ausatmen und einatmen
        // nach abzug dc-offset: hoher druck -> niedriger druck
        // vorzeichenwechsel + nach -
        if (v.get(0) < -minChange) {
            return false;
        }

        boolean ausatmenErkannt = false;

        for (double d : v) {
            // ausatmen
            if (d > minChange) {
                ausatmenErkannt = true;
            }
            // einatmen
            if (d < -minChange && ausatmenErkannt) {
                Log.d("breathmode-a1", d + " < " + -minChange);
                return true;
            }
        }
        return false;
    }

    /**
     * Detects breath mode A2 "wiederholtes ausatmen und einatmen"
     *
     * @param v             list of measurements
     * @param minChange     minimum change to react to (noise filtering)
     * @param requiredCount minimum number of cycles
     * @return true if mode detected, false otherwise
     */
    private boolean detectBreathModeA2(List<Double> v, double minChange, int requiredCount) {
        // wiederholtes ausatmen und einatmen
        // nach abzug dc-offset: mehrere vorzeichenwechsel

        int count = 0;

        for (int i = 1; i < v.size(); i++) {
            double lastValue = v.get(i - 1);
            double currValue = v.get(i);

            if (lastValue < -minChange && currValue > minChange) {
                // event detected
                count++;
            }
        }

        if (count >= requiredCount) {
            return true;
        }

        return false;
    }

    /**
     * Detects breath mode A3b "luft anhalten, ausatmen"
     *
     * @param v            list of measurements
     * @param minChange    minimum change to react to (noise filtering)
     * @param minCountHold minimum number of data points required to detect "hold breath"
     * @return true if mode detected, false otherwise
     */
    private boolean detectBreathModeA3a(List<Double> v, double minChange, int minCountHold) {
        // Luft anhalten, dann ausatmen
        // Nach Abzug dc-offset wert um 0, dann positiv

        int holdCount = 0;
        boolean modeHold = true;
        for (int i = 0; i < v.size(); i++) {
            double currValue = v.get(i);
            if (modeHold) {
                if (Math.abs(currValue) < minChange) {
                    // hold breath
                    holdCount++;
                } else {
                    // stopped holding breath
                    modeHold = false;
                    if (holdCount < minCountHold) {
                        // failed to reach minimum hold count
                        return false;
                    }
                }
            } else {
                if (currValue < -minChange) {
                    // einatmen
                    return false;
                } else if (currValue > minChange) {
                    // ausatmen
                    return true;
                }
            }
        }


        return false;

    }

    /**
     * Detects breath mode A3b "luft anhalten, einatmen"
     *
     * @param v            list of measurements
     * @param minChange    minimum change to react to (noise filtering)
     * @param minCountHold minimum number of data points required to detect "hold breath"
     * @return true if mode detected, false otherwise
     */
    private boolean detectBreathModeA3b(List<Double> v, double minChange, int minCountHold) {
        // Luft anhalten, einatmen
        // Nach Abzug dc-offset wert um 0, dann negativ

        int holdCount = 0;
        boolean modeHold = true;
        for (int i = 0; i < v.size(); i++) {
            double currValue = v.get(i);
            if (modeHold) {
                if (Math.abs(currValue) < minChange) {
                    // hold breath
                    holdCount++;
                } else {
                    // stopped holding breath
                    modeHold = false;
                    if (holdCount < minCountHold) {
                        // failed to reach minimum hold count
                        return false;
                    }
                }
            } else {
                if (currValue < -minChange) {
                    // einatmen
                    return true;
                } else if (currValue > minChange) {
                    // ausatmen
                    return false;
                }
            }
        }


        return false;
    }

    /**
     * Detects breath mode A4a, "stufenweise ausatmen"
     *
     * @param v              list of measurements
     * @param minChange      minimum change to react to (noise filtering)
     * @param minCountHold   minimum number of data points required for detecting "hold breath"
     * @param minBreathCount minimum number of data points required for detecting "breath action"
     * @param minSteps       minimum number of hold-breath-breath-action cycles
     * @return true if mode detected, false otherwise
     */
    private boolean detectBreathModeA4a(List<Double> v, double minChange, int minCountHold, int minBreathCount, int minSteps) {
        // stufenweise ausatmen = wiederholtes anhalten und ausatmen

        // state machine
        // 0: hold breath ---> 1:ausatmen					wenn hc > mCH und cV > mC
        // 0: hold breath ---> 0: hold breath und hc = 0	wenn hc <= mCH und cV > mC
        // 1: ausatmen --> 0: hold breath und sC++ und hc=0	wenn cV < mC

        // Luft anhalten, dann ausatmen
        // Nach Abzug dc-offset wert um 0, dann positiv

        int stepCount = 0;

        int holdCount = 0;
        int breathCount = 0;
        boolean modeHold = true;
        boolean modeBreath = false;
        for (int i = 0; i < v.size(); i++) {
            double currValue = v.get(i);
            if (modeHold) {
                if (Math.abs(currValue) < minChange) {
                    // hold breath
                    holdCount++;
                } else {
                    // stopped holding breath
                    if (holdCount < minCountHold) {
                        // failed to reach minimum hold count
                        holdCount = 0;
                    } else {
                        // reached minimum hold count --> transition to next state
                        modeHold = false;
                        modeBreath = true;
                        breathCount = 0;
                    }
                }
            } else if (modeBreath) {
                if (currValue > minChange) {
                    // ausatmen
                    breathCount++;
                } else if (currValue < -minChange) {
                    // einatmen, FALSCH
                    // transition to start state
                    breathCount = 0;
                    holdCount = 0;
                    modeHold = false;
                    modeBreath = true;
                } else {
                    // hold?
                    if (breathCount >= minBreathCount) {
                        // detected a step
                        stepCount++;
                    }
                    // transition to start state
                    modeHold = true;
                    modeBreath = false;
                    holdCount = 0;
                    breathCount = 0;
                }
            }
        }

        if (stepCount >= minSteps) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Detects breath mode A4b, "stufenweise einatmen"
     *
     * @param v              list of measurements
     * @param minChange      minimum change to react to (noise filtering)
     * @param minCountHold   minimum number of data points required for detecting "hold breath"
     * @param minBreathCount minimum number of data points required for detecting "breath action"
     * @param minSteps       minimum number of hold-breath-breath-action cycles
     * @return true if mode detected, false otherwise
     */
    private boolean detectBreathModeA4b(List<Double> v, double minChange, int minCountHold, int minBreathCount, int minSteps) {
        // stufenweise einatmen = wiederholtes anhalten und einatmen

        // state machine
        // 0: hold breath ---> 1:einatmen					wenn hc > mCH und cV < mC
        // 0: hold breath ---> 0: hold breath und hc = 0	wenn hc <= mCH und cV < mC
        // 1: ausatmen --> 0: hold breath und sC++ und hc=0	wenn cV > mC

        // Luft anhalten, dann ausatmen
        // Nach Abzug dc-offset wert um 0, dann positiv

        int stepCount = 0;

        int holdCount = 0;
        int breathCount = 0;
        boolean modeHold = true;
        boolean modeBreath = false;
        for (int i = 0; i < v.size(); i++) {
            double currValue = v.get(i);
            if (modeHold) {
                if (Math.abs(currValue) < minChange) {
                    // hold breath
                    holdCount++;
                } else {
                    // stopped holding breath
                    if (holdCount < minCountHold) {
                        // failed to reach minimum hold count
                        holdCount = 0;
                    } else {
                        // reached minimum hold count --> transition to next state
                        modeHold = false;
                        modeBreath = true;
                        breathCount = 0;
                    }
                }
            } else if (modeBreath) {
                if (currValue < -minChange) {
                    // einatmen
                    breathCount++;
                } else if (currValue > minChange) {
                    // ausatmen, FALSCH
                    // transition to start state, no step detected
                    breathCount = 0;
                    holdCount = 0;
                    modeHold = false;
                    modeBreath = true;
                } else {
                    // hold?
                    if (breathCount >= minBreathCount) {
                        // detected a step
                        stepCount++;
                    }
                    // transition to start state
                    modeHold = true;
                    modeBreath = false;
                    holdCount = 0;
                    breathCount = 0;
                }
            }
        }

        if (stepCount >= minSteps) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Detects breath mode "einatmen"
     *
     * @param v            List of values
     * @param mc           min change to consider (noise filtering)
     * @param firstNValues use the most recent n values for mode detection
     * @return true if mode detected, false otherwise
     */
    private boolean detectBreathModeEinatmen(List<Double> v, double mc, int firstNValues) {
        for (int i = v.size() - firstNValues; i < v.size() && i >= 0; i++) {
            if (v.get(i) > -mc) {
                return false;
            }
        }

        return true;
    }


    /**
     * Detects breath mode "ausatmen"
     *
     * @param v            List of values
     * @param mc           min change to consider (noise filtering)
     * @param firstNValues use the most recent n values for mode detection
     * @return true if mode detected, false otherwise
     */
    private boolean detectBreathModeAusatmen(List<Double> v, double mc, int firstNValues) {
        for (int i = v.size() - firstNValues; i < v.size() && i >= 0; i++) {
            if (v.get(i) < mc) {
                return false;
            }
        }

        return true;

    }

    public Set<BreathMode> getBreathModes() {
        Set<BreathMode> ret = new HashSet<BreathMode>();
        List<Double> tmp = removeOffset(values, getDcOffset());

        Log.d("bsdp", "dc offset: " + getDcOffset());
        if (detectBreathModeEinatmen(tmp, MINCHANGE, 5)) {
            ret.add(BreathMode.EINATMEN);
        }
        if (detectBreathModeAusatmen(tmp, MINCHANGE, 5)) {
            ret.add(BreathMode.AUSATMEN);
        }

        if (detectBreathModeA1(tmp, MINCHANGE)) {
            ret.add(BreathMode.AUSATMEN_UND_EINATMEN);
        }
        if (detectBreathModeA2(tmp, MINCHANGE / 4, 2)) {
            ret.add(BreathMode.WIEDERHOLTES_AUSATMEN_UND_EINATMEN);
        }
        if (detectBreathModeA3a(tmp, MINCHANGE, 3)) {
            ret.add(BreathMode.LUFT_ANHALTEN_AUSATMEN);
        }
        if (detectBreathModeA3b(tmp, MINCHANGE, 3)) {
            ret.add(BreathMode.LUFT_ANHALTEN_EINATMEN);
        }
        if (detectBreathModeA4a(tmp, MINCHANGE, 3, 1, 2)) {
            ret.add(BreathMode.STUFENWEISE_AUSATMEN);
        }
        if (detectBreathModeA4b(tmp, MINCHANGE, 3, 1, 2)) {
            ret.add(BreathMode.STUFENWEISE_EINATMEN);
        }

        return ret;
    }
}

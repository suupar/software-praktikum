package unimr.swp_impl_wg.sensor;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.UUID;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

/**
 * @author Felix Rieger
 */
public class BluetoothAdapter implements Serializable {

    private InputStream inStream;
    private BluetoothSocket btSock;
    private final String bluetoothAddress;
    private boolean connected;

    public boolean openBluetoothConnection() {
        android.bluetooth.BluetoothAdapter BA;
        BA = android.bluetooth.BluetoothAdapter.getDefaultAdapter();
        BA.enable();

        BluetoothDevice dev = BA.getRemoteDevice(bluetoothAddress);

        try {
            btSock = dev.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
            btSock.connect();
            inStream = btSock.getInputStream();
            connected = true;
            return true;
        } catch (IOException e) {
            connected = false;
            return false;
        }

    }

    public boolean closeBluetoothConnection() {
        try {
            if (btSock == null) {
                return false;
            }
            btSock.close();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public BluetoothAdapter(String bluetoothAddress) {
        super();
        this.bluetoothAddress = bluetoothAddress;
    }

    public InputStream getInStream() {
        return inStream;
    }

    public boolean isConnected() {
        return connected;
    }


}

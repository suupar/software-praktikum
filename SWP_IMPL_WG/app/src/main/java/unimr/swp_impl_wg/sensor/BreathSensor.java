package unimr.swp_impl_wg.sensor;

import java.util.Observable;

/**
 * @author Felix Rieger
 */
public abstract class BreathSensor extends Observable {
    public abstract boolean connect();

    public abstract void startSensor();

    public abstract double getPressure();
}

package unimr.swp_impl_wg.sensor;

import java.io.InputStream;
import java.util.InputMismatchException;
import java.util.Locale;
import java.util.Scanner;


import android.util.Log;

/**
 * @author Felix Rieger
 */
public class HighPerformanceBreathSensor extends BreathSensor {
    private final String bluetoothAddress;
    private BluetoothAdapter ba;
    private boolean connSuccess;
    private boolean started = false;
    private double currentTemperature;
    private double currentPressure;

    /**
     * Create a new breath sensor
     *
     * @param bluetoothAddress address of the breath sensor
     */
    public HighPerformanceBreathSensor(String bluetoothAddress) {
        super();
        this.bluetoothAddress = bluetoothAddress;
    }

    /**
     * Connect to the sensor using bluetooth
     *
     * @return
     */
    public boolean connect() {
        Log.d("bt-sensor", "opening bluetooth connection to " + bluetoothAddress);
        if (ba == null) {
            ba = new BluetoothAdapter(bluetoothAddress);
        }
        if (!ba.isConnected()) {
            connSuccess = ba.openBluetoothConnection();
        }
        Log.d("bt-sensor", "success? " + connSuccess);
        return connSuccess;
    }

    @Override
    public void startSensor() {
        Log.d("bt-sensor", "starting sensor.");
        if (started) {
            Log.d("bt-sensor", "sensor already started.");
            return;
        }
        started = true;
        final InputStream sensorInputStream = ba.getInStream();
        if (sensorInputStream == null) {
            Log.e("bt-sensor", "could not open input stream");
            return;
        }

        Runnable inputStreamReaderRunnable = new Runnable() {

            @Override
            public void run() {
                if (sensorInputStream == null) {
                    return;
                } else {
                    Scanner sc = new Scanner(sensorInputStream);
                    while (sc.hasNextLine()) {
                        String inStreamString = sc.nextLine();
                        Scanner sc2 = new Scanner(inStreamString).useLocale(Locale.US);
                        if (sc2 != null) {
                            try {
                                currentTemperature = sc2.nextDouble();
                                currentPressure = sc2.nextDouble();
                                setChanged();
                                notifyObservers(currentPressure);
                            } catch (InputMismatchException e) {
                                // do nothing
                            }
                        }
                        sc2.close();
                    }
                }
            }
        };

        // start the reader thread
        Thread isrThread = new Thread(inputStreamReaderRunnable);
        isrThread.start();
    }

    public double getCurrentTemperature() {
        return currentTemperature;
    }

    public double getPressure() {
        return currentPressure;
    }


}

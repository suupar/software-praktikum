package unimr.swp_impl_wg.Helper;

/**
 * @author Fabio Geiger, Tim Waldhans
 */

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;


import java.io.File;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;


import java.io.OutputStream;


/**
 * This Class reads and handles the Questions Database (Json/CSV)
 */

public class FileCopyTools {

    /**
     * Copies Files from Streams
     *
     * @param in  InputStream to be copied
     * @param out Outputstream to be copied in
     * @throws IOException IO Exception
     * @see InputStream
     * @see OutputStream
     */
    public static void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    /**
     * This Method copies the original File
     * @param context passed Context
     *                Copies questions.json from Assets
     *                to /data/data/files
     * @throws IOException
     */

    public static void copyAssets(Context context) {
        AssetManager assetManager = context.getAssets();
        InputStream in = null;
        OutputStream out = null;
        String filename = "questions.json";
        try {
            in = assetManager.open(filename);

            String outDir = context.getFilesDir().getAbsolutePath();

            File outFile = new File(outDir, filename);
            out = new FileOutputStream(outFile);
            FileCopyTools.copyFile(in, out);
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        } catch (IOException e) {
            Log.e("tag", "Failed to copy asset file: " + filename, e);
        }

    }

}






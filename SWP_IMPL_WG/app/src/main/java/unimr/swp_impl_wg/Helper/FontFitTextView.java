package unimr.swp_impl_wg.Helper;
import android.content.Context;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.TypedValue;


/**
 * @author Tim Waldhans, Fabio Geiger
 * This class aligns Textviews in Activities
 */



public class FontFitTextView extends android.support.v7.widget.AppCompatTextView {

    /**
     * @param Paint holds the style and color information about how to draw geometries, text and bitmaps.
     */
    private Paint mTestPaint;

    /**
     * Constructor
     * @param context passed context
     */

    public FontFitTextView(Context context) {
        super(context);
        initialise();
    }

    /**
     * Constructor
     * @param context passed context
     * @param attrs Attribute Set, handles XML data
     */

    public FontFitTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialise();
    }

    /**
     * initialises Instance
     */

    private void initialise() {
        mTestPaint = new Paint();
        mTestPaint.set(this.getPaint());

    }

    /**
     * Aligns text in view
     * @param text      passed Text
     * @param textWidth passed Text measurements
     */


    private void refitText(String text, int textWidth)
    {
        if (textWidth <= 0)
            return;
        int targetWidth = textWidth - this.getPaddingLeft() - this.getPaddingRight();
        float hi = 100;
        float lo = 2;
        final float threshold = 0.5f; // How close we have to be

        mTestPaint.set(this.getPaint());

        while((hi - lo) > threshold) {
            float size = (hi+lo)/2;
            mTestPaint.setTextSize(size);
            if(mTestPaint.measureText(text) >= targetWidth)
                hi = size; // too big
            else
                lo = size; // too small
        }
        this.setTextSize(TypedValue.COMPLEX_UNIT_PX, lo);
    }

    /**
     * Refits Text Size in Activity
     * @param widthMeasureSpec      Width measure
     * @param heightMeasureSpec     Height measure
     */

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
        int height = getMeasuredHeight();
        refitText(this.getText().toString(), parentWidth);
        this.setMeasuredDimension(parentWidth, height);
    }

    /**
     * Called, when text is changed, aligns text
     * @param text      passed text
     * @param start     overridden
     * @param before    overridden
     * @param after     overriden
     */

    @Override
    protected void onTextChanged(final CharSequence text, final int start, final int before, final int after) {
        refitText(text.toString(), this.getWidth());
    }

    /**
     * called when ViewSize is changed
     * @param w     New passed width
     * @param h     overridden
     * @param old  old width
     * @param oldh  overridden
     */

    @Override
    protected void onSizeChanged (int w, int h, int old, int oldh) {
        if (w != old) {
            refitText(this.getText().toString(), w);
        }
    }


}

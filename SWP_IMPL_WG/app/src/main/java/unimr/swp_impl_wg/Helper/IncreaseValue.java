package unimr.swp_impl_wg.Helper;

/**
 * * @author Tim Waldhans, Fabio Geiger
 */

import android.os.Handler;
import android.util.Log;
import android.widget.NumberPicker;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static android.content.ContentValues.TAG;

/**
 * This class Increments given Values timed with Runnable of the
 * passed Numberpicker
 * @see NumberPicker
 * @see Runnable
 * @see Handler
 */

public class IncreaseValue {

    /**
     * @param counter        Global Counter var
     * @param picker         NumberPicker manipulate
     * @param incrementValue Value to Increment
     * @param increment      Global boolean var for checling incrementability
     * @param handler        Handler for Runnables
     * @param fire           Inner Runnable Class for delay
     */
    private int counter = 0;
    private final NumberPicker picker;
    private final int incrementValue;
    private final boolean increment;

    private final Handler handler = new Handler();
    private final Runnable fire = new Runnable() { @Override public void run() { fire(); } };


    /**
     * Constructor
     * @param picker            passed NumberPicker
     * @param incrementValue    passed incrementValue for this instance
     */

    public IncreaseValue(final NumberPicker picker, final int incrementValue) {
        this.picker = picker;
        if (incrementValue > 0) {
            increment = true;
            this.incrementValue = incrementValue;
        } else {
            increment = false;
            this.incrementValue = -incrementValue;
        }
    }

    /**
     *  Executes Runnable after given delay
     * @param startDelay Delay passed in milliseconds
     */

    public void run(final int startDelay) {
        handler.postDelayed(fire, startDelay);  // This will execute the runnable passed to it (fire)
        // after [startDelay in milliseconds], ASYNCHRONOUSLY.
    }

    /**
     * Increments value with Runnable var
     */

    private void fire() {
        ++counter;
        if (counter > incrementValue) return;

        try {
            // reflection call for
            // picker.changeValueByOne(true);
            final Method method = picker.getClass().getDeclaredMethod("changeValueByOne", boolean.class);
            method.setAccessible(true);
            method.invoke(picker, increment);

        } catch (final NoSuchMethodException | InvocationTargetException |
                IllegalAccessException | IllegalArgumentException e) {
            Log.d(TAG, "...", e);
        }

        handler.postDelayed(fire, 120);  // This will execute the runnable passed to it (fire)
    }
}

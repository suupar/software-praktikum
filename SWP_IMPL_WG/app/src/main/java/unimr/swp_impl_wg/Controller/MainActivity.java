package unimr.swp_impl_wg.Controller;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;
import java.util.Set;


import unimr.swp_impl_wg.Model.Category;
import unimr.swp_impl_wg.Helper.FileCopyTools;
import unimr.swp_impl_wg.Model.PointTools;
import unimr.swp_impl_wg.Model.Question;
import unimr.swp_impl_wg.Model.QuestionData;
import unimr.swp_impl_wg.Model.SensorData;
import unimr.swp_impl_wg.R;
import unimr.swp_impl_wg.sensor.BreathMode;
import unimr.swp_impl_wg.sensor.BreathSensor;
import unimr.swp_impl_wg.sensor.BreathSensorDataProcessor;
import unimr.swp_impl_wg.sensor.HighPerformanceBreathSensor;

/**
 * @author Fabio Geiger, Tim Waldhans
 *         Launcher Activity
 */

public class MainActivity extends AppCompatActivity {

    /**
     * @param btSensorAdress Adress for Bluetooth Adapter
     * @param hpSensorAdress Adress for Data Processor
     * @param valcount Offset data
     */
    private static final String btSensorAddress = "20:15:08:10:46:23";
    private static final String hpSensorAddress = "00:14:03:18:22:30";
    private int valCount = 0;

    /**
     * @param savedInstanceState SavedInstanceState
     * @see Bundle
     * This Class represents the First View to Start a Quiz
     * Breath movements as DropDown menu included
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final SharedPreferences prefs = this.getSharedPreferences("com.quizapy.app", Context.MODE_PRIVATE);
        final TextView mode = (TextView) findViewById(R.id.textView18);
        BreathSensor sensor = new HighPerformanceBreathSensor(hpSensorAddress);

        if (!prefs.contains("GameMode")) {
            prefs.edit().putInt("GameMode", 3).apply();
        }
        if (!prefs.contains("QuestionsPerRound")) {
            prefs.edit().putInt("QuestionsPerRound", 10).apply();
        }
        if (!prefs.contains("BT")) {
            prefs.edit().putBoolean("BT", false).apply();
        }
        if (prefs.getBoolean("BT", false)) {
            btConnect(sensor);
        }
        /** reads in Questions CSV
         *
         */
        if (!new File(getFilesDir().getAbsolutePath(), "questions.json").exists()) {
            FileCopyTools.copyAssets(this);

        }
        QuestionData.getInstance().empty();
        final File questions = new File(getFilesDir().getAbsolutePath(), "questions.json");
        if (!validateJSON()) {
            Toast.makeText(this, "Es gab einen Fehler beim überprüfen der Fragen. Stellen sie die Originalen Dateien über die Einstellungen wieder her.", Toast.LENGTH_LONG).show();
        } else {
            int test = QuestionData.getInstance().readGameDataFromJson(this);
            QuestionData.getInstance().setNumberofQuestions(test);
        }
        QuestionData.setUnanswered(prefs, QuestionData.getInstance().getNumberofQuestions());

        if (!prefs.contains("InitialPoints")) {
            PointTools.setPoints(0, 6, prefs);
        }
        if (!prefs.contains("PSQ1")) {
            prefs.edit().putInt("PSQ1", 0).apply();
            prefs.edit().putInt("PSQ2", 0).apply();
            prefs.edit().putInt("PSQ3", 0).apply();
        }
        String concat = "Gewählter Atemmodus: " + prefs.getInt("GameMode", 1);
        mode.setText(concat);
        /**
         * Breath Controler
         */

        final Button ausatmen = (Button) findViewById(R.id.bOUT1);
        ausatmen.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToConfig();
            }
        });

        final Button einatmen = (Button) findViewById(R.id.bIN1);
        einatmen.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (PointTools.getAllPlayingPoints(prefs) == 0) {
                    if (PointTools.getPoints(1, prefs) > 0) {
                        Toast.makeText(MainActivity.this, "Nicht genug Punkte um zu Spielen. Es sind noch Punkte im SafeActivity verfügbar.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MainActivity.this, "Nicht genug Punkte um zu Spielen", Toast.LENGTH_SHORT).show();
                    }

                } else if (QuestionData.getUnanswered(prefs) < prefs.getInt("QuestionsPerRound", 10) * 2) {
                    Toast.makeText(MainActivity.this, "Nicht genug Fragen um zu Spielen", Toast.LENGTH_SHORT).show();
                } else {
                    goToCategories();
                }
            }
        });

        final Button testsafe = (Button) findViewById(R.id.buttonsafe);
        testsafe.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSafe();
            }
        });

    }


    /**
     * Starts QuestionData Activity
     */

    private void goToCategories() {
        Intent intent = new Intent(this, ChooseCategoriesActivity.class);
        startActivity(intent);
    }

    /**
     * Starts Configuration Activity
     */
    private void goToConfig() {
        Intent intent = new Intent(this, ConfigActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * starts Safe Activity
     */

    private void goToSafe() {
        Intent intent = new Intent(this, SafeActivity.class);
        startActivity(intent);
    }

    /**
     * Connects to Bueltooth Adapter
     * @param sensor Passed Breath sensor Instance
     */

    private void btConnect(BreathSensor sensor) {
        final TextView tv = (TextView) findViewById(R.id.textView7);
        final TextView tv1 = (TextView) findViewById(R.id.textView8);
        try {
            Log.d("bs conn", "bs connect? --> " + sensor.connect());
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "Bluetooth Verbindung fehlgeschlagen!", Toast.LENGTH_SHORT).show();
        }

        sensor.startSensor();
        sensor.addObserver(new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                final double d = (Double) arg;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String setText = "---> " + d;
                        tv.setText(setText);
                    }
                });
            }
        });
        BreathSensorDataProcessor dataproc = new BreathSensorDataProcessor(sensor, 30);
        dataproc.addObserver(new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                valCount++;
                Log.d("bdsp-vc", "..." + valCount);
                BreathSensorDataProcessor dp = (BreathSensorDataProcessor) o;
                if (valCount == 100) {
                    dp.enableDynamicDcOffset(false);
                    dp.setDcOffset(dp.computeDcOffset());
                }
                Set<BreathMode> bm = dp.getBreathModes();

                StringBuilder sb = new StringBuilder();
                for (BreathMode b : bm) {
                    sb.append(b.name()).append("\n");
                }
                final String disp = sb.toString();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (valCount < 110) {
                            tv1.setText("WAIT, CALIBRATING...");
                        } else {
                            String setText = ">" + disp;
                            tv1.setText(setText);
                        }
                    }
                });
            }
        });
        tv1.setVisibility(View.INVISIBLE);
        SensorData.getInstance().setSensor(sensor);
        SensorData.getInstance().setBp(dataproc);
    }

    /**
     * This Method validates the JSON input
     * @return True, if valid
     */

    private boolean validateJSON() {
        final File questions = new File(getFilesDir().getAbsolutePath(), "questions.json");
        try {
            InputStream is = new FileInputStream(questions);
            Scanner sc = new Scanner(is);
            StringBuilder sb = new StringBuilder();
            while (sc.hasNextLine()) {
                sb.append(sc.nextLine());
            }
            sc.close();
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
            QuestionData dataJSON = mapper.readValue(sb.toString(), QuestionData.class);
            for (Category c : dataJSON.getCategories()) {
                if (c.getCategoryName().equals("")) {
                    return false;
                }
                if (c.level1.isEmpty() || c.level2.isEmpty() || c.level3.isEmpty()) {
                    return false;
                }
                for (int i = 0; i < c.level1.size(); i++) {
                    Question q = c.level1.get(i);
                    if (q.getAnsw()[0].equals("") || q.getAnsw()[1].equals("") || q.getAnsw()[2].equals("") || q.getQuestion().equals("") || q.getAnsw_true().equals("")) {
                        return false;
                    }
                }
                for (int i = 0; i < c.level2.size(); i++) {
                    Question q = c.level2.get(i);
                    if (q.getAnsw()[0].equals("") || q.getAnsw()[1].equals("") || q.getAnsw()[2].equals("") || q.getQuestion().equals("") || q.getAnsw_true().equals("")) {
                        return false;
                    }
                }
                for (int i = 0; i < c.level3.size(); i++) {
                    Question q = c.level3.get(i);
                    if (q.getAnsw()[0].equals("") || q.getAnsw()[1].equals("") || q.getAnsw()[2].equals("") || q.getQuestion().equals("") || q.getAnsw_true().equals("")) {
                        return false;
                    }
                }
            }
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }
}


package unimr.swp_impl_wg.Controller;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import unimr.swp_impl_wg.Model.PointTools;
import unimr.swp_impl_wg.Model.QuestionArrayAdapter;
import unimr.swp_impl_wg.Model.QuestionData;
import unimr.swp_impl_wg.R;

/**
 * This class displays Answered Questions marked as wrong/correct
 *
 * @author Tim Waldhans, Fabio Geiger
 */

public class ResultViewActivity extends AppCompatActivity {


    public int getRight() {
        return right;
    }

    public void setRight(int right) {
        this.right = right;
    }

    /**
     * Counter for correctly answered questions for points
     */

    private int right = 0;


    /**
     * Innitiates View and handles its displaying of Answered Questions
     * Corrects ansered Questions in questions DataBase
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_view);
        final Button Reset = (Button) findViewById(R.id.button2);
        final TextView p = (TextView) findViewById(R.id.textView15);
        final ListView listview = (ListView) findViewById(R.id.listview);
        final SharedPreferences prefs = this.getSharedPreferences("com.quizapy.app", Context.MODE_PRIVATE);
        final int cat = getIntent().getIntExtra("Cat", 0), diff = getIntent().getIntExtra("Diff", 0);
        final QuestionArrayAdapter adapter = new QuestionArrayAdapter(this, QuestionData.getInstance().getPickedQuestions(), cat);
        for (int i = 0; i < QuestionData.getInstance().getPickedQuestions().length; i++) {
            if (QuestionData.getInstance().getPickedQuestions()[i].answeredCorrect()) {
                setRight(getRight() + 1);
            }
        }
        listview.setAdapter(adapter);
        listview.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                listview.removeOnLayoutChangeListener(this);
                p.setText("Verloren! -" + diff + " Punkt/e.\nRichtig beantwortet: " + getRight() + "/" + prefs.getInt("QuestionsPerRound", 10));
                if (getRight() >= (prefs.getInt("QuestionsPerRound", 10) / 2)) {
                    p.setText("GEWONNEN! +" + diff + " Punkt/e.\nRichtig beantwortet: " + getRight() + "/" + prefs.getInt("QuestionsPerRound", 10));
                }


            }
        });
        adapter.notifyDataSetChanged();


        Reset.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {


                QuestionData.getInstance().updateJSONFile(QuestionData.getInstance().getPickedQuestions(), cat, diff, getApplicationContext());

                if (getRight() >= (prefs.getInt("QuestionsPerRound", 10) / 2)) {
                    PointTools.setPoints(2, PointTools.getPoints(2, prefs) + diff, prefs);
                } else {
                    PointTools.lostPoints(prefs, diff);
                }

                goToMain(findViewById(R.id.content_main));

            }
        });

    }


    /**
     * Goes back to main
     *
     * @param view passed View
     * @see View
     */

    public void goToMain(View view) {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
    }

    /**
     * This method jumps back to the Main Activity with new Intent
     *
     * @see Intent
     */
    @Override
    public void onBackPressed() {
        Intent i = new Intent(ResultViewActivity.this, MainActivity.class);
        startActivity(i);
        super.onBackPressed();
    }
}

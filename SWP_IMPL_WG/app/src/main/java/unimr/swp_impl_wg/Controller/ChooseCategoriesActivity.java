package unimr.swp_impl_wg.Controller;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;

import android.widget.Button;

import android.widget.RadioButton;
import android.widget.RadioGroup;

import android.widget.Toast;

import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;

import unimr.swp_impl_wg.Model.QuestionData;
import unimr.swp_impl_wg.Model.SensorData;
import unimr.swp_impl_wg.R;
import unimr.swp_impl_wg.sensor.BreathMode;
import unimr.swp_impl_wg.sensor.BreathSensorDataProcessor;

/**
 * @author Fabio Geiger, Tim Waldhans
 *         This class handles Question QuestionData and display the matchin activity
 */

public class ChooseCategoriesActivity extends AppCompatActivity {
    private List<Integer> radioids;

    private CountDownTimer cdt;

    /**
     * OnCreate Method, initiate RadioButtons for Category picking, their Highlighting and Onclick
     * Listener such as Toast feedback.
     * Breath movements as DropDown menu included
     * It also filters out Questions and QuestionData with an unsufficient amount of Questions
     *
     * @param savedInstanceState SavedInstanceState
     * @see Bundle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);

        /*
         * Radio Button View
         */
        final RadioGroup radiogr = (RadioGroup) findViewById(R.id.radiogr1);
        RadioGroup.LayoutParams rprms;
        Random rn = new Random();
        int rndint;
        radioids = new LinkedList<>();
        for (int i = 0; i < QuestionData.getInstance().getCategories().size(); i++) {
            rndint = rn.nextInt(5) + 1;
            RadioButton radioButton = new RadioButton(this);
            radioButton.setText(QuestionData.getInstance().getCategories().get(i).getCategoryName());
            radioButton.setId((i + 100) * rndint);
            radioButton.setTextSize(24f);
            radioButton.setClickable(false);
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                radioButton.setPadding(80, 20, 0, 20);
            }

            radioids.add(radioButton.getId());

            radiogr.addView(radioButton);

        }
        radiogr.check(radioids.get(0));
        radiogr.setClickable(false);
        final SharedPreferences prefs = this.getSharedPreferences("com.quizapy.app", Context.MODE_PRIVATE);


        cdt = new CountDownTimer(Integer.MAX_VALUE, 1500) { //40000 milli seconds is total time, 1000 milli seconds is time interval

            public void onTick(long millisUntilFinished) {
                if (radioids.indexOf(radiogr.getCheckedRadioButtonId()) + 1 < radioids.size()) {
                    radiogr.check(radioids.get(radioids.indexOf(radiogr.getCheckedRadioButtonId()) + 1));
                } else {
                    radiogr.check(radioids.get(0));
                }

            }

            public void onFinish() {
                cancel();
                start();
            }
        }.start();


        final Button ausatmen = (Button) findViewById(R.id.bOUT2);
        ausatmen.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                cdt.onFinish();
            }
        });

        final Button einatmen = (Button) findViewById(R.id.bIN2);
        einatmen.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                cdt.cancel();
                int qincat = 0;
                for (int i = 0; i < QuestionData.getInstance().getCategories().get(radioids.indexOf(radiogr.getCheckedRadioButtonId())).level1.size(); i++) {
                    if (!QuestionData.getInstance().getCategories().get(radioids.indexOf(radiogr.getCheckedRadioButtonId())).level1.get(i).answered()) {
                        qincat++;
                    }
                }
                for (int i = 0; i < QuestionData.getInstance().getCategories().get(radioids.indexOf(radiogr.getCheckedRadioButtonId())).level2.size(); i++) {
                    if (!QuestionData.getInstance().getCategories().get(radioids.indexOf(radiogr.getCheckedRadioButtonId())).level2.get(i).answered()) {
                        qincat++;
                    }
                }
                for (int i = 0; i < QuestionData.getInstance().getCategories().get(radioids.indexOf(radiogr.getCheckedRadioButtonId())).level3.size(); i++) {
                    if (!QuestionData.getInstance().getCategories().get(radioids.indexOf(radiogr.getCheckedRadioButtonId())).level3.get(i).answered()) {
                        qincat++;
                    }
                }
                if (qincat < prefs.getInt("QuestionsPerRound", 10)) {
                    Toast.makeText(ChooseCategoriesActivity.this, "Nicht genügend Fragen in dieser Kategorie", Toast.LENGTH_SHORT).show();
                } else {
                    goToChoosePoints(radioids.indexOf(radiogr.getCheckedRadioButtonId()));
                    Toast.makeText(ChooseCategoriesActivity.this, QuestionData.getInstance().getCategories().get(radioids.indexOf(radiogr.getCheckedRadioButtonId())).getCategoryName() + " wurde gewählt", Toast.LENGTH_SHORT).show();
                }
            }
        });
        if (prefs.getBoolean("BT", false)) {
            SensorData.getInstance().getBp().addObserver(new Observer() {
                @Override
                public void update(Observable o, Object arg) {
                    BreathSensorDataProcessor dp = (BreathSensorDataProcessor) o;
                    if (dp.getBreathModes().contains(BreathMode.EINATMEN)) {
                        einatmen.callOnClick();
                    } else if (dp.getBreathModes().contains(BreathMode.AUSATMEN)) {
                        ausatmen.callOnClick();
                    }
                }
            });
        }


    }


    /**
     * This method jumps to the GamePoint picking Activity
     *
     * @param cat Passed Category
     */

    public void goToChoosePoints(int cat) {
        Intent intent = new Intent(this, ChoosePointsActivity.class);
        intent.putExtra("Cat", cat);
        startActivity(intent);
    }

}

package unimr.swp_impl_wg.Controller;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import unimr.swp_impl_wg.Helper.IncreaseValue;
import unimr.swp_impl_wg.R;

/**
 * @author Tim Waldhans, Fabio Geiger
 */

public class SetPINActivity extends AppCompatActivity {

    /**
     * @param pressing  Boolean value for checking pressed Button
     * @param exHandler Global Handler fpr Runnable handling
     * @param count     Global counter Int for Points
     */

    boolean pressing;
    final Handler exHandler = new Handler();
    int count;

    /**
     * Create function
     * Declares Buttons, Numberpicker, Layout, OnClickListener
     *
     * @param savedInstanceState passed InstanceState
     * @see Bundle
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_safe);
        count = 0;
        final NumberPicker np1 = (NumberPicker) findViewById(R.id.numberPicker1);
        final Button bIn = (Button) findViewById(R.id.button1);
        final Button bOut = (Button) findViewById(R.id.button3);

        String[] nums = new String[10];
        for (int i = 0; i < nums.length; i++) {
            nums[i] = Integer.toString(i);
        }
        np1.setDisplayedValues(nums);
        np1.setMinValue(0);
        np1.setMaxValue(9);
        np1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        bIn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        pressing = true;
                        schedulePeriodicIncrease();
                        break;
                    case MotionEvent.ACTION_UP:
                        pressing = false;
                        stopPeriodicIncrease();
                        break;
                }
                return true;
            }
        });

        bOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final TextView tv1 = (TextView) findViewById(R.id.textView3);
                final TextView tv2 = (TextView) findViewById(R.id.textView2);
                final TextView tv3 = (TextView) findViewById(R.id.textView1);
                final SharedPreferences prefs = getSharedPreferences("com.quizapy.app", Context.MODE_PRIVATE);
                String value;
                switch (count) {
                    case 0:
                        value = "" + np1.getValue();
                        tv1.setText(value);
                        count++;
                        break;
                    case 1:
                        value = "" + np1.getValue();
                        tv2.setText(value);
                        count++;
                        break;
                    case 2:
                        value = "" + np1.getValue();
                        tv3.setText(value);
                        count++;
                        break;
                    case 3:
                        prefs.edit().putInt("PSQ1", Integer.parseInt(tv1.getText().toString())).apply();
                        prefs.edit().putInt("PSQ2", Integer.parseInt(tv2.getText().toString())).apply();
                        prefs.edit().putInt("PSQ3", Integer.parseInt(tv3.getText().toString())).apply();
                        tv1.setText("-");
                        tv2.setText("-");
                        tv3.setText("-");
                        Toast.makeText(SetPINActivity.this, "Safe PIN festgelegt!", Toast.LENGTH_SHORT).show();
                        count = 0;
                        goToConfig();

                }


            }
        });


    }

    /**
     * Execution Handler
     *
     * @see Handler
     */

    public void schedulePeriodicIncrease() {
        exHandler.post(execution);
    }

    /**
     * Execution Handler
     *
     * @see Handler
     */

    public void stopPeriodicIncrease() {
        exHandler.removeCallbacks(execution);
    }


    /**
     * Runnable Var
     */

    private Runnable execution = new Runnable() {
        @Override
        public void run() {
            if (!pressing) return;
            final NumberPicker np1 = (NumberPicker) findViewById(R.id.numberPicker1);
            incrementByOne(np1);
            exHandler.postDelayed(execution, 600);
        }
    };

    /**
     * This Method handles the Point increment
     *
     * @param np NumberPicker to increment
     */


    private void incrementByOne(NumberPicker np) {
        IncreaseValue inc = new IncreaseValue(np, 1);
        inc.run(50);
    }

    /**
     * Goes to ConfigActivity Activity
     */

    private void goToConfig() {
        Intent intent = new Intent(this, ConfigActivity.class);
        startActivity(intent);
        finish();
    }
}

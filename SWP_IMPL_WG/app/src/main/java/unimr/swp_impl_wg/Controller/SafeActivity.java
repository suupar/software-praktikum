package unimr.swp_impl_wg.Controller;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;


import java.util.Observable;
import java.util.Observer;

import unimr.swp_impl_wg.Helper.IncreaseValue;
import unimr.swp_impl_wg.Model.SensorData;
import unimr.swp_impl_wg.R;
import unimr.swp_impl_wg.sensor.BreathMode;
import unimr.swp_impl_wg.sensor.BreathSensorDataProcessor;

/**
 * @author Tim Waldhans, Fabio Geiger
 *         This Class displays the SafeActivity Opening Activity
 */
public class SafeActivity extends AppCompatActivity {

    /**
     * @param pressing   Boolean value for checking pressed Button
     * @param exHandler  Global Handler fpr Runnable handling
     * @param count      Global counter Int that goes to 3 and shows which part of the PIN is going to be entered
     * @param values     Array for PIN
     */
    boolean pressing;
    final Handler exHandler = new Handler();
    int count;
    int[] values = new int[3];

    /**
     * Create function
     * Declares Buttons, Numberpicker, Layout, OnClickListener
     *
     * @param savedInstanceState passed InstanceState
     * @see Bundle
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_safe);
        count = 0;
        final NumberPicker np1 = (NumberPicker) findViewById(R.id.numberPicker1);
        final Button bIn = (Button) findViewById(R.id.button1);
        final Button bOut = (Button) findViewById(R.id.button3);
        final SharedPreferences prefs = getSharedPreferences("com.quizapy.app", Context.MODE_PRIVATE);
        String[] nums = new String[10];
        for (int i = 0; i < nums.length; i++) {
            nums[i] = Integer.toString(i);
        }
        np1.setDisplayedValues(nums);
        np1.setMinValue(0);
        np1.setMaxValue(9);
        np1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        bIn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        pressing = true;
                        schedulePeriodicIncrease();
                        break;
                    case MotionEvent.ACTION_UP:
                        pressing = false;
                        stopPeriodicIncrease();
                        break;
                }
                return true;
            }
        });

        bOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final TextView tv1 = (TextView) findViewById(R.id.textView3);
                final TextView tv2 = (TextView) findViewById(R.id.textView2);
                final TextView tv3 = (TextView) findViewById(R.id.textView1);
                final SharedPreferences prefs = getSharedPreferences("com.quizapy.app", Context.MODE_PRIVATE);
                String value;

                switch (count) {
                    case 0:
                        value = "" + np1.getValue();
                        values[count] = np1.getValue();
                        tv1.setText(value);
                        count++;
                        break;
                    case 1:
                        value = "" + np1.getValue();
                        values[count] = np1.getValue();
                        tv2.setText(value);
                        count++;
                        break;
                    case 2:
                        value = "" + np1.getValue();
                        values[count] = np1.getValue();
                        tv3.setText(value);
                        count++;
                        break;
                    case 3:
                        if (checkPIN(prefs, values)) {
                            goToUnlocked();
                        } else {
                            tv1.setText("-");
                            tv2.setText("-");
                            tv3.setText("-");
                            Toast.makeText(SafeActivity.this, "Falscher PIN!", Toast.LENGTH_SHORT).show();
                            count = 0;
                        }
                }


            }
        });
        if (prefs.getBoolean("BT", false)) {
            SensorData.getInstance().getBp().addObserver(new Observer() {
                @Override
                public void update(Observable o, Object arg) {
                    long downTime = SystemClock.uptimeMillis();
                    long eventTime = SystemClock.uptimeMillis() + 100;
                    float x = 0.0f;
                    float y = 0.0f;
                    int metaState = 0;
                    MotionEvent event = MotionEvent.obtain(downTime, eventTime, MotionEvent.ACTION_DOWN, x, y, metaState);
                    BreathSensorDataProcessor dp = (BreathSensorDataProcessor) o;
                    if (dp.getBreathModes().contains(BreathMode.EINATMEN)) {
                        bIn.dispatchTouchEvent(event);
                    } else if (dp.getBreathModes().contains(BreathMode.AUSATMEN)) {
                        bOut.callOnClick();
                    }
                }
            });
        }

    }

    /**
     * Execution Handler
     *
     * @see Handler
     */

    public void schedulePeriodicIncrease() {
        exHandler.post(execution);
    }

    /**
     * Execution Handler
     *
     * @see Handler
     */

    public void stopPeriodicIncrease() {
        exHandler.removeCallbacks(execution);
    }


    /**
     * Runnable Var
     */

    private Runnable execution = new Runnable() {
        @Override
        public void run() {
            if (!pressing) return;
            final NumberPicker np1 = (NumberPicker) findViewById(R.id.numberPicker1);
            incrementByOne(np1);
            exHandler.postDelayed(execution, 600);
        }
    };

    /**
     * This Method handles the Point increment
     *
     * @param np NumberPicker to increment
     */

    private void incrementByOne(NumberPicker np) {
        IncreaseValue inc = new IncreaseValue(np, 1);
        inc.run(50);
    }

    /**
     * This Method checks PIN validity
     *
     * @param prefs Passed Preferences for Pin
     * @param input Input PIN
     * @return True, if Pin is valid
     */

    private boolean checkPIN(SharedPreferences prefs, int[] input) {
        int[] pin = {prefs.getInt("PSQ1", 0), prefs.getInt("PSQ2", 0), prefs.getInt("PSQ3", 0)};
        return pin[0] == input[0] && pin[1] == input[1] && pin[2] == input[2];

    }

    /**
     * This Method starts the SafeActivity Activity if the PIN is valid
     */

    private void goToUnlocked() {
        Intent intent = new Intent(this, SafeUnlockedActivity.class);
        startActivity(intent);
        finish();
    }


}

package unimr.swp_impl_wg.Controller;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Observable;
import java.util.Observer;
import java.util.Random;

import unimr.swp_impl_wg.Model.PointTools;
import unimr.swp_impl_wg.Model.QuestionData;
import unimr.swp_impl_wg.Model.SensorData;
import unimr.swp_impl_wg.R;
import unimr.swp_impl_wg.sensor.BreathMode;
import unimr.swp_impl_wg.sensor.BreathSensorDataProcessor;

/**
 * This Activity lets the user Pick his Game Points
 */
public class ChoosePointsActivity extends AppCompatActivity {


    /**
     * @param savedInstanceState SavedInstanceState
     * @see Bundle
     * Handles GamePoint picking by ProgressBar
     * notifies if not enough points are available
     * Breath movements included
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_points);

        final SharedPreferences prefs = this.getSharedPreferences("com.quizapy.app", Context.MODE_PRIVATE);
        final ProgressBar pbar = (ProgressBar) findViewById(R.id.progressBar);
        final TextView tv = (TextView) findViewById(R.id.textView2);
        final TextView points = (TextView) findViewById(R.id.textView16);
        pbar.setMax(3);
        pbar.setProgress(1);
        final int allPoints = PointTools.getAllPlayingPoints(prefs);
        points.setText("Punkte verfügbar: " + allPoints);
        final int cat = getIntent().getIntExtra("Cat", 0);


        //Ausatmen
        final Button ausatmen = (Button) findViewById(R.id.bOUt3);
        ausatmen.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Auf nächste Seite gehen
                int diff = 0, qinlevel;
                if (pbar.getProgress() == 1) {
                    diff = 1;
                    qinlevel = 0;
                    for (int i = 0; i < QuestionData.getInstance().getCategories().get(cat).level1.size(); i++) {
                        if (!QuestionData.getInstance().getCategories().get(cat).level1.get(i).answered()) {
                            qinlevel++;
                        }
                    }
                    if (allPoints < 1) {
                        Toast.makeText(ChoosePointsActivity.this, "Nicht genügend Punkte vorhanden", Toast.LENGTH_SHORT).show();
                    } else if (qinlevel < prefs.getInt("QuestionsPerRound", 10)) {
                        Toast.makeText(ChoosePointsActivity.this, "Nicht genügend Fragen in dieser Schwierigkeitsstufe", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ChoosePointsActivity.this, "1 Punkt eingesetzt", Toast.LENGTH_SHORT).show();
                        goToQuizView(cat, diff, prefs.getInt("GameMode", 3));
                    }
                } else if (pbar.getProgress() == 2) {
                    diff = 2;
                    qinlevel = 0;
                    for (int i = 0; i < QuestionData.getInstance().getCategories().get(cat).level2.size(); i++) {
                        if (!QuestionData.getInstance().getCategories().get(cat).level2.get(i).answered()) {
                            qinlevel++;
                        }
                    }
                    if (allPoints < 2) {
                        Toast.makeText(ChoosePointsActivity.this, "Nicht genügend Punkte vorhanden", Toast.LENGTH_SHORT).show();
                    } else if (qinlevel < prefs.getInt("QuestionsPerRound", 10)) {
                        Toast.makeText(ChoosePointsActivity.this, "Nicht genügend Fragen in dieser Schwierigkeitsstufe", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ChoosePointsActivity.this, "2 Punkte eingesetzt", Toast.LENGTH_SHORT).show();
                        goToQuizView(cat, diff, prefs.getInt("GameMode", 3));
                    }
                } else if (pbar.getProgress() == 3) {
                    diff = 3;
                    qinlevel = 0;
                    for (int i = 0; i < QuestionData.getInstance().getCategories().get(cat).level3.size(); i++) {
                        if (!QuestionData.getInstance().getCategories().get(cat).level3.get(i).answered()) {
                            qinlevel++;
                        }
                    }
                    if (allPoints < 3) {
                        Toast.makeText(ChoosePointsActivity.this, "Nicht genügend Punkte vorhanden", Toast.LENGTH_SHORT).show();
                    } else if (qinlevel < prefs.getInt("QuestionsPerRound", 10)) {
                        Toast.makeText(ChoosePointsActivity.this, "Nicht genügend Fragen in dieser Schwierigkeitsstufe", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ChoosePointsActivity.this, "3 Punkte eingesetzt", Toast.LENGTH_SHORT).show();
                        goToQuizView(cat, diff, prefs.getInt("GameMode", 3));
                    }
                }

            }
        });

        //Einatmen
        final Button einatmen = (Button) findViewById(R.id.bIN3);
        einatmen.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Punkeeinsatz erhöhen
                if (pbar.getProgress() == 3) {
                    pbar.setProgress(1);
                    tv.setText("1/3 Punkte Einsatz");
                    return;
                }
                pbar.incrementProgressBy(1);
                String temp = pbar.getProgress() + "/3 Punkte Einsatz";
                tv.setText(temp);

            }
        });

        if (prefs.getBoolean("BT", false)) {
            SensorData.getInstance().getBp().addObserver(new Observer() {
                @Override
                public void update(Observable o, Object arg) {
                    BreathSensorDataProcessor dp = (BreathSensorDataProcessor) o;
                    if (dp.getBreathModes().contains(BreathMode.EINATMEN)) {
                        einatmen.callOnClick();
                    } else if (dp.getBreathModes().contains(BreathMode.AUSATMEN)) {
                        ausatmen.callOnClick();
                    }
                }
            });
        }

    }

    /**
     * Swaps Activity to QuizView
     *
     * @param cat  passed Category
     * @param diff passed difficulty
     * @param mode passed GameMode
     * @see View
     */

    public void goToQuizView(int cat, int diff, int mode) {
        switch (mode) {
            case 1:
                Intent QuizView1 = new Intent(this, QuizView1Activity.class);
                QuizView1.putExtra("Cat", cat);
                QuizView1.putExtra("Diff", diff);
                startActivity(QuizView1);
                break;
            case 2:
                Intent QuizView2 = new Intent(this, QuizView2Activity.class);
                QuizView2.putExtra("Cat", cat);
                QuizView2.putExtra("Diff", diff);
                startActivity(QuizView2);
                break;
            case 3:
                Intent QuizView3 = new Intent(this, QuizView3Activity.class);
                QuizView3.putExtra("Cat", cat);
                QuizView3.putExtra("Diff", diff);
                startActivity(QuizView3);
                break;
            case 4:
                Intent QuizView4 = new Intent(this, QuizView4Activity.class);
                QuizView4.putExtra("Cat", cat);
                QuizView4.putExtra("Diff", diff);
                startActivity(QuizView4);
                break;
            case 5:
                int rand = new Random().nextInt(4) + 1;
                switch (rand) {
                    case 1:
                        Intent QuizView1rnd = new Intent(this, QuizView1Activity.class);
                        QuizView1rnd.putExtra("Cat", cat);
                        QuizView1rnd.putExtra("Diff", diff);
                        startActivity(QuizView1rnd);
                        break;
                    case 2:
                        Intent QuizView2rnd = new Intent(this, QuizView2Activity.class);
                        QuizView2rnd.putExtra("Cat", cat);
                        QuizView2rnd.putExtra("Diff", diff);
                        startActivity(QuizView2rnd);
                        break;
                    case 3:
                        Intent QuizView3rnd = new Intent(this, QuizView3Activity.class);
                        QuizView3rnd.putExtra("Cat", cat);
                        QuizView3rnd.putExtra("Diff", diff);
                        startActivity(QuizView3rnd);
                        break;
                    case 4:
                        Intent QuizView4rnd = new Intent(this, QuizView4Activity.class);
                        QuizView4rnd.putExtra("Cat", cat);
                        QuizView4rnd.putExtra("Diff", diff);
                        startActivity(QuizView4rnd);
                        break;
                }
        }
    }


}

package unimr.swp_impl_wg.Controller;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;


import unimr.swp_impl_wg.Helper.FileCopyTools;
import unimr.swp_impl_wg.Model.QuestionData;
import unimr.swp_impl_wg.R;


public class ConfigActivity extends AppCompatActivity {
    /**
     * @param savedInstanceState SavedInstanceState
     * @see Bundle
     * Handles Configuration picking by Buttons
     * Resets Questions
     * Breath movements included
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final TextView questsT = (TextView) findViewById(R.id.CountDisplay);
        final RadioGroup rg = (RadioGroup) findViewById(R.id.RadioGroup1);
        final Button qPerRound = (Button) findViewById(R.id.QuestionCount1);
        final Button reset = (Button) findViewById(R.id.resetq);
        final Button SetPin = (Button) findViewById(R.id.setsafepin);
        final ToggleButton toggleBluetooth = (ToggleButton) findViewById(R.id.bluetoothToggle);
        final SharedPreferences prefs = getSharedPreferences("com.quizapy.app", Context.MODE_PRIVATE);
        String setText = "" + prefs.getInt("QuestionsPerRound", 10);
        questsT.setText(setText);


        switch (prefs.getInt("GameMode", 0)) {
            case 1:
                rg.check(R.id.radioButton1);
                break;
            case 2:
                rg.check(R.id.radioButton2);
                break;
            case 3:
                rg.check(R.id.radioButton3);
                break;
            case 4:
                rg.check(R.id.radioButton4);
                break;
            case 5:
                rg.check(R.id.radioButton5);
                break;

        }
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                SharedPreferences prefs = getSharedPreferences("com.quizapy.app", Context.MODE_PRIVATE);
                switch (checkedId) {
                    case R.id.radioButton1:
                        prefs.edit().putInt("GameMode", 1).apply();
                        Toast.makeText(ConfigActivity.this, "Atemmodus 1 gewählt", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radioButton2:
                        prefs.edit().putInt("GameMode", 2).apply();
                        Toast.makeText(ConfigActivity.this, "Atemmodus 2 gewählt", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radioButton3:
                        prefs.edit().putInt("GameMode", 3).apply();
                        Toast.makeText(ConfigActivity.this, "Atemmodus 3 gewählt", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radioButton4:
                        prefs.edit().putInt("GameMode", 4).apply();
                        Toast.makeText(ConfigActivity.this, "Atemmodus 4 gewählt", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radioButton5:
                        prefs.edit().putInt("GameMode", 5).apply();
                        Toast.makeText(ConfigActivity.this, "Atemmodus 5 gewählt", Toast.LENGTH_SHORT).show();
                        break;

                }
            }
        });

        qPerRound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int questProRound;
                final SharedPreferences prefs = getSharedPreferences("com.quizapy.app", Context.MODE_PRIVATE);
                questProRound = prefs.getInt("QuestionsPerRound", 10);
                if (questProRound < 30) {
                    int test = prefs.getInt("QuestionsPerRound", 10);
                    test++;
                    prefs.edit().putInt("QuestionsPerRound", test).apply();
                    String concat = "" + prefs.getInt("QuestionsPerRound", 10);
                    questsT.setText(concat);
                } else {
                    prefs.edit().putInt("QuestionsPerRound", 10).apply();
                    String concat = "" + prefs.getInt("QuestionsPerRound", 10);
                    questsT.setText(concat);
                }
            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SharedPreferences prefs = getSharedPreferences("com.quizapy.app", Context.MODE_PRIVATE);
                prefs.edit().clear().apply();
                FileCopyTools.copyAssets(ConfigActivity.this);
                QuestionData.getInstance().setNumberofQuestions(0);
                Toast.makeText(ConfigActivity.this, "Daten und Einstellungen wurden zurückgesetzt!", Toast.LENGTH_SHORT).show();
                startMain();
            }
        });

        SetPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSetPin();
            }
        });
        if (prefs.getBoolean("BT", false)) {
            toggleBluetooth.setChecked(true);
        } else {
            toggleBluetooth.setChecked(false);
        }
        toggleBluetooth.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (toggleBluetooth.isChecked()) {
                    prefs.edit().putBoolean("BT", true).apply();
                } else if (!toggleBluetooth.isChecked()) {
                    prefs.edit().putBoolean("BT", false).apply();
                    android.bluetooth.BluetoothAdapter BA = android.bluetooth.BluetoothAdapter.getDefaultAdapter();
                    if (BA.isEnabled()) {
                        BA.disable();
                    }
                }
            }
        });
        TextView unanswered = (TextView) findViewById(R.id.unanswered);
        String text = "Anzahl unbeantworteter Fragen: " + QuestionData.getUnanswered(prefs);
        unanswered.setText(text);
    }

    /**
     * Passes Intent when back Button is pressed
     */
    @Override
    public void onBackPressed() {
        Intent i = new Intent(ConfigActivity.this, MainActivity.class);
        startActivity(i);
        super.onBackPressed();
    }

    /**
     * restarts App on StartView
     */
    private void startMain() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    /**
     * Starts SetPin Activity
     */

    private void goToSetPin() {
        Intent intent = new Intent(this, SetPINActivity.class);
        startActivity(intent);
    }


}




package unimr.swp_impl_wg.Controller;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.Observable;
import java.util.Observer;
import java.util.Random;

import unimr.swp_impl_wg.Helper.DebouncedOnClickListener;
import unimr.swp_impl_wg.Helper.FontFitTextView;
import unimr.swp_impl_wg.Model.Question;
import unimr.swp_impl_wg.Model.QuestionData;
import unimr.swp_impl_wg.Model.SensorData;
import unimr.swp_impl_wg.R;
import unimr.swp_impl_wg.sensor.BreathMode;
import unimr.swp_impl_wg.sensor.BreathSensorDataProcessor;

/**
 * This class Handles the Question answering sequence 1
 *
 * @author Tim Waldhans, fabio Geiger
 */

public class QuizView1Activity extends AppCompatActivity {

    /**
     * @param questions Represents Questions sequence
     * @param rand Global random int field
     * @param doubleBackToExitPressedOnce Boolean for pressed Button
     * @param einatmenClicked boolean for checking if Button is pressed
     */

    private Question[] questions;
    private int rand;
    boolean doubleBackToExitPressedOnce = false;

    boolean einatmenClicked;

    public int getRand() {
        return rand;
    }

    public void setRand(int rand) {
        this.rand = rand;
    }


    /**
     * @param savedInstanceState SavedInstanceState
     * @see Bundle
     * This Class represents the Quizview for BreathMode 1
     * Breath movements included
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_view1);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Layout functions
        final ToggleButton Answer1 = (ToggleButton) findViewById(R.id.AW1t);
        final ToggleButton Answer2 = (ToggleButton) findViewById(R.id.AW2t);
        final ToggleButton Answer3 = (ToggleButton) findViewById(R.id.AW3t);
        final ToggleButton Answer4 = (ToggleButton) findViewById(R.id.AW4t);
        final int cat = getIntent().getIntExtra("Cat", 0), diff = getIntent().getIntExtra("Diff", 0);
        final ProgressBar progress = (ProgressBar) findViewById(R.id.Mode1Bar);
        final TextView currMode = (TextView) findViewById(R.id.CurrentMode);

        progress.setVisibility(View.INVISIBLE);
        currMode.setText("Wählen und halten");
        // Breath simulator


        final Button einatmen = (Button) findViewById(R.id.bIN4);
        final SharedPreferences prefs = this.getSharedPreferences("com.quizapy.app", MODE_PRIVATE);
        einatmen.setOnClickListener(new DebouncedOnClickListener(200) {
            final ToggleButton Answer1 = (ToggleButton) findViewById(R.id.AW1t);
            final ToggleButton Answer2 = (ToggleButton) findViewById(R.id.AW2t);
            final ToggleButton Answer3 = (ToggleButton) findViewById(R.id.AW3t);
            final ToggleButton Answer4 = (ToggleButton) findViewById(R.id.AW4t);
            @Override
            public void onDebouncedClick(View v) {
                if (Answer1.isChecked()) {
                    Answer1.setChecked(false);
                    Answer2.setChecked(true);
                    refreshQuestion(getQuestions()[getI()]);
                } else if (Answer2.isChecked()) {
                    Answer2.setChecked(false);
                    Answer3.setChecked(true);
                    refreshQuestion(getQuestions()[getI()]);
                } else if (Answer3.isChecked()) {
                    Answer3.setChecked(false);
                    Answer4.setChecked(true);
                    refreshQuestion(getQuestions()[getI()]);
                } else if (Answer4.isChecked()) {
                    Answer4.setChecked(false);
                    Answer1.setChecked(true);
                    refreshQuestion(getQuestions()[getI()]);
                }
            }
        });


        final Button ausatmen = (Button) findViewById(R.id.bOUT4);
        progress.setMax(100);
        progress.setProgress(1);

        ausatmen.setOnTouchListener(new Button.OnTouchListener() {

            private Handler handler = new Handler();

            @Override
            public boolean onTouch(View v, final MotionEvent event) {

                handler.postDelayed(new Runnable() {
                    public void run() {

                        progress.setVisibility(View.VISIBLE);
                        progress.incrementProgressBy(1);

                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            progress.setVisibility(View.INVISIBLE);
                            progress.setProgress(0);
                        }
                        if (progress.getProgress() == progress.getMax()) {
                            progress.setVisibility(View.INVISIBLE);
                            progress.setProgress(0);

                            if (Answer1.isChecked()) {
                                questionAnswered(getQuestions(), Answer1.getText().toString());
                                if (!getQuestions()[prefs.getInt("QuestionsPerRound", 10) - 1].getGivenAnswer().equals("")) {
                                    QuestionData.getInstance().setPickedQuestions(getQuestions());
                                    goToResultView(cat, diff);
                                }

                            }
                            if (Answer2.isChecked()) {
                                questionAnswered(getQuestions(), Answer2.getText().toString());
                                if (!getQuestions()[prefs.getInt("QuestionsPerRound", 10) - 1].getGivenAnswer().equals("")) {
                                    QuestionData.getInstance().setPickedQuestions(getQuestions());
                                    goToResultView(cat, diff);
                                }

                            }
                            if (Answer3.isChecked()) {
                                questionAnswered(getQuestions(), Answer3.getText().toString());
                                if (!getQuestions()[prefs.getInt("QuestionsPerRound", 10) - 1].getGivenAnswer().equals("")) {
                                    QuestionData.getInstance().setPickedQuestions(getQuestions());
                                    goToResultView(cat, diff);
                                }
                            }
                            if (Answer4.isChecked()) {
                                questionAnswered(getQuestions(), Answer4.getText().toString());
                                if (!getQuestions()[prefs.getInt("QuestionsPerRound", 10) - 1].getGivenAnswer().equals("")) {
                                    QuestionData.getInstance().setPickedQuestions(getQuestions());
                                    goToResultView(cat, diff);
                                }
                            }

                        }


                    }
                }, 100);

                return true;
            }

        });

        Question[] questions = QuestionData.pickQuestions(cat, diff, prefs);
        //Ausgewählter button wird zufällig gewählt
        Random rn = new Random();
        int toggled = rn.nextInt(4) + 1;
        switch (toggled)

        {
            case 1:
                Answer1.toggle();
                break;
            case 2:
                Answer2.toggle();
                break;
            case 3:
                Answer3.toggle();
                break;
            case 4:
                Answer4.toggle();
                break;
        }
        setQuestions(questions);
        displayQuestion(getQuestions()[getI()]);
        if (prefs.getBoolean("BT", false)) {
            SensorData.getInstance().getBp().addObserver(new Observer() {
                @Override
                public void update(Observable o, Object arg) {
                    long downTime = SystemClock.uptimeMillis();
                    long eventTime = SystemClock.uptimeMillis() + 100;
                    float x = 0.0f;
                    float y = 0.0f;
                    int metaState = 0;
                    MotionEvent event = MotionEvent.obtain(downTime, eventTime, MotionEvent.ACTION_DOWN, x, y, metaState);
                    BreathSensorDataProcessor dp = (BreathSensorDataProcessor) o;
                    if (dp.getBreathModes().contains(BreathMode.EINATMEN)) {
                        einatmen.callOnClick();
                    } else if (dp.getBreathModes().contains(BreathMode.AUSATMEN)) {
                        ausatmen.dispatchTouchEvent(event);
                    } else {
                        event = MotionEvent.obtain(downTime, eventTime, MotionEvent.ACTION_UP, x, y, metaState);
                        ausatmen.dispatchTouchEvent(event);
                    }
                }
            });
        }

    }


    /**
     * Manages Questions and Updates question Array with answered questions
     *
     * @param qs QuestionArray
     * @param a  postion
     */
    private void questionAnswered(Question[] qs, String a) {
        final SharedPreferences prefs = this.getSharedPreferences("com.quizapy.app", Context.MODE_PRIVATE);
        qs[getI()].setGivenAnswer(a);
        if (getI() < prefs.getInt("QuestionsPerRound", 10) - 1) {

            setI(getI() + 1);
            displayQuestion(qs[getI()]);
        }
        setQuestions(qs);

    }

    /**
     * Displays New Question in QuizView
     *
     * @param q Question to be displayed
     */
    private void displayQuestion(Question q) {
        final FontFitTextView Question = (FontFitTextView) findViewById(R.id.Question);
        final ToggleButton Answer1 = (ToggleButton) findViewById(R.id.AW1t);
        final ToggleButton Answer2 = (ToggleButton) findViewById(R.id.AW2t);
        final ToggleButton Answer3 = (ToggleButton) findViewById(R.id.AW3t);
        final ToggleButton Answer4 = (ToggleButton) findViewById(R.id.AW4t);
        setRand(new Random().nextInt(3) + 1);
        switch (getRand()) {
            case 1:
                Question.setText(q.getQuestion());
                Answer1.setText(q.getAnsw_true());
                Answer2.setText(q.getAnsw()[1]);
                Answer3.setText(q.getAnsw()[2]);
                Answer4.setText(q.getAnsw()[0]);
                break;
            case 2:
                Question.setText(q.getQuestion());
                Answer4.setText(q.getAnsw_true());
                Answer3.setText(q.getAnsw()[1]);
                Answer2.setText(q.getAnsw()[2]);
                Answer1.setText(q.getAnsw()[0]);
                break;
            case 3:
                Question.setText(q.getQuestion());
                Answer2.setText(q.getAnsw_true());
                Answer4.setText(q.getAnsw()[1]);
                Answer1.setText(q.getAnsw()[2]);
                Answer3.setText(q.getAnsw()[0]);
                break;
        }


    }

    /**
     * Refreshes Question display for each Button
     *
     * @param q Passed Question Database class
     */

    private void refreshQuestion(Question q) {
        final FontFitTextView Question = (FontFitTextView) findViewById(R.id.Question);
        final ToggleButton Answer1 = (ToggleButton) findViewById(R.id.AW1t);
        final ToggleButton Answer2 = (ToggleButton) findViewById(R.id.AW2t);
        final ToggleButton Answer3 = (ToggleButton) findViewById(R.id.AW3t);
        final ToggleButton Answer4 = (ToggleButton) findViewById(R.id.AW4t);
        switch (rand) {
            case 1:
                Question.setText(q.getQuestion());
                Answer1.setText(q.getAnsw_true());
                Answer2.setText(q.getAnsw()[1]);
                Answer3.setText(q.getAnsw()[2]);
                Answer4.setText(q.getAnsw()[0]);
                break;
            case 2:
                Question.setText(q.getQuestion());
                Answer4.setText(q.getAnsw_true());
                Answer3.setText(q.getAnsw()[1]);
                Answer2.setText(q.getAnsw()[2]);
                Answer1.setText(q.getAnsw()[0]);
                break;
            case 3:
                Question.setText(q.getQuestion());
                Answer2.setText(q.getAnsw_true());
                Answer4.setText(q.getAnsw()[1]);
                Answer1.setText(q.getAnsw()[2]);
                Answer3.setText(q.getAnsw()[0]);
                break;
        }
    }

    /**
     * Passes Questions to Result view
     */
    private void goToResultView(int cat, int diff) {
        Intent i = new Intent(this, ResultViewActivity.class);
        i.putExtra("Cat", cat);
        i.putExtra("Diff", diff);
        startActivity(i);
        finish();
    }

    /**
     * @return returns field i
     */
    public int getI() {
        return i;
    }

    /**
     * @param i sets field i
     */

    public void setI(int i) {
        this.i = i;
    }

    /**
     * @param i Counter for questions to be shown
     */

    private int i = 0;


    public Question[] getQuestions() {
        return questions;
    }

    /**
     * setter
     *
     * @param questions Question Array
     */

    public void setQuestions(Question[] questions) {
        this.questions = questions;
    }

    /**
     * This Method handles the back button, jumps back to MainActivity
     */

    @Override
    public void onBackPressed() {
        Intent i = new Intent(QuizView1Activity.this, MainActivity.class);
        if (doubleBackToExitPressedOnce) {
            startActivity(i);
            super.onBackPressed();
            finish();
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Nochmal drücken um abzubrechen", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);

    }

}

package unimr.swp_impl_wg.Controller;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.Observable;
import java.util.Observer;
import java.util.Random;

import unimr.swp_impl_wg.Helper.DebouncedOnClickListener;
import unimr.swp_impl_wg.Helper.FontFitTextView;
import unimr.swp_impl_wg.Model.Question;
import unimr.swp_impl_wg.Model.QuestionData;
import unimr.swp_impl_wg.Model.SensorData;
import unimr.swp_impl_wg.R;
import unimr.swp_impl_wg.sensor.BreathMode;
import unimr.swp_impl_wg.sensor.BreathSensorDataProcessor;

/**
 * This class Handles the Question answering sequence 3
 * @author Tim Waldhans, fabio Geiger
 */

public class QuizView3Activity extends AppCompatActivity {


    /**
     * @param questions Represents Questions sequence
     * @param i Counter for questions to be shown
     * @param rand Global random int
     * @param doubleBackToExitPressedOnce boolean vor Back-Button Handling
     */

    private int i = 0;
    private Question[] questions;
    private int rand;
    boolean doubleBackToExitPressedOnce = false;

    public int getRand() {
        return rand;
    }

    public void setRand(int rand) {
        this.rand = rand;
    }


    /**
     * OnCreate function, manages Question pick with Breath movements
     *
     * @param savedInstanceState InstanceState
     * @see Bundle
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_view3);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar);
        // Layout functions
        final ToggleButton Answer1 = (ToggleButton) findViewById(R.id.AW1t);
        final ToggleButton Answer2 = (ToggleButton) findViewById(R.id.AW2t);
        final ToggleButton Answer3 = (ToggleButton) findViewById(R.id.AW3t);
        final ToggleButton Answer4 = (ToggleButton) findViewById(R.id.AW4t);
        final int cat = getIntent().getIntExtra("Cat", 0), diff = getIntent().getIntExtra("Diff", 0);
        final SharedPreferences prefs = this.getSharedPreferences("com.quizapy.app", MODE_PRIVATE);
        final TextView currMode = (TextView) findViewById(R.id.CurrentMode);
        currMode.setText("Stufenlos wählen");


        // Breath simulator

        final Button ausatmen = (Button) findViewById(R.id.bOUT4);
        ausatmen.setOnClickListener(new DebouncedOnClickListener(200) {

            @Override
            public void onDebouncedClick(View v) {
                // TODO Auto-generated method stub
                if (Answer1.isChecked()) {
                    questionAnswered(getQuestions(), Answer1.getText().toString());
                    if (!getQuestions()[prefs.getInt("QuestionsPerRound", 10) - 1].getGivenAnswer().equals("")) {
                        QuestionData.getInstance().setPickedQuestions(getQuestions());
                        goToResultView(cat, diff);
                    }

                }
                if (Answer2.isChecked()) {
                    questionAnswered(getQuestions(), Answer2.getText().toString());
                    if (!getQuestions()[prefs.getInt("QuestionsPerRound", 10) - 1].getGivenAnswer().equals("")) {
                        QuestionData.getInstance().setPickedQuestions(getQuestions());
                        goToResultView(cat, diff);
                    }

                }
                if (Answer3.isChecked()) {
                    questionAnswered(getQuestions(), Answer3.getText().toString());
                    if (!getQuestions()[prefs.getInt("QuestionsPerRound", 10) - 1].getGivenAnswer().equals("")) {
                        QuestionData.getInstance().setPickedQuestions(getQuestions());
                        goToResultView(cat, diff);
                    }
                }
                if (Answer4.isChecked()) {
                    questionAnswered(getQuestions(), Answer4.getText().toString());
                    if (!getQuestions()[prefs.getInt("QuestionsPerRound", 10) - 1].getGivenAnswer().equals("")) {
                        QuestionData.getInstance().setPickedQuestions(getQuestions());
                        goToResultView(cat, diff);
                    }
                }

            }
        });


        final Button einatmen = (Button) findViewById(R.id.bIN4);
        einatmen.setOnTouchListener(mode3());
        unimr.swp_impl_wg.Model.Question[] questions = QuestionData.pickQuestions(cat, diff, prefs);
        //Ausgewählter button wird zufällig gewählt
        Random rn = new Random();
        int toggled = rn.nextInt(4) + 1;
        switch (toggled)

        {
            case 1:
                Answer1.toggle();
                break;
            case 2:
                Answer2.toggle();
                break;
            case 3:
                Answer3.toggle();
                break;
            case 4:
                Answer4.toggle();
                break;
        }
        setQuestions(questions);
        displayQuestion(getQuestions()[getI()]);
        if (prefs.getBoolean("BT", false)) {
            SensorData.getInstance().getBp().addObserver(new Observer() {
                BreathMode first;

                @Override
                public void update(Observable o, Object arg) {
                    long downTime = SystemClock.uptimeMillis();
                    long eventTime = SystemClock.uptimeMillis() + 100;
                    float x = 0.0f;
                    float y = 0.0f;
                    int metaState = 0;
                    MotionEvent event = MotionEvent.obtain(downTime, eventTime, MotionEvent.ACTION_DOWN, x, y, metaState);
                    BreathSensorDataProcessor dp = (BreathSensorDataProcessor) o;
                    if (dp.getBreathModes().contains(BreathMode.EINATMEN)) {
                        if (first == null) {
                            first = BreathMode.EINATMEN;
                        }
                        if (first == BreathMode.AUSATMEN) {
                            ausatmen.callOnClick();
                            return;
                        }
                        einatmen.dispatchTouchEvent(event);
                    } else if (dp.getBreathModes().contains(BreathMode.AUSATMEN)) {
                        if (first == null) {
                            first = BreathMode.AUSATMEN;
                        }
                        if (first == BreathMode.EINATMEN) {
                            ausatmen.callOnClick();
                            return;
                        }
                        einatmen.dispatchTouchEvent(event);
                    }
                }
            });
        }

    }

    /**
     * Manages Questions and Updates question Array with answered questions
     *
     * @param qs QuestionArray
     * @param a  postion
     */
    private void questionAnswered(Question[] qs, String a) {
        final SharedPreferences prefs = this.getSharedPreferences("com.quizapy.app", Context.MODE_PRIVATE);
        qs[getI()].setGivenAnswer(a);
        if (getI() < prefs.getInt("QuestionsPerRound", 10) - 1) {

            setI(getI() + 1);
            displayQuestion(qs[getI()]);
        }
        setQuestions(qs);

    }

    /**
     * Displays New Question in QuizView
     *
     * @param q Question to be displayed
     */
    private void displayQuestion(Question q) {
        final FontFitTextView Question = (FontFitTextView) findViewById(R.id.Question);
        final ToggleButton Answer1 = (ToggleButton) findViewById(R.id.AW1t);
        final ToggleButton Answer2 = (ToggleButton) findViewById(R.id.AW2t);
        final ToggleButton Answer3 = (ToggleButton) findViewById(R.id.AW3t);
        final ToggleButton Answer4 = (ToggleButton) findViewById(R.id.AW4t);
        setRand(new Random().nextInt(3) + 1);
        switch (getRand()) {
            case 1:
                Question.setText(q.getQuestion());
                Answer1.setText(q.getAnsw_true());
                Answer2.setText(q.getAnsw()[1]);
                Answer3.setText(q.getAnsw()[2]);
                Answer4.setText(q.getAnsw()[0]);
                break;
            case 2:
                Question.setText(q.getQuestion());
                Answer4.setText(q.getAnsw_true());
                Answer3.setText(q.getAnsw()[1]);
                Answer2.setText(q.getAnsw()[2]);
                Answer1.setText(q.getAnsw()[0]);
                break;
            case 3:
                Question.setText(q.getQuestion());
                Answer2.setText(q.getAnsw_true());
                Answer4.setText(q.getAnsw()[1]);
                Answer1.setText(q.getAnsw()[2]);
                Answer3.setText(q.getAnsw()[0]);
                break;
        }


    }

    /**
     * Refreshes Question display for each Button
     *
     * @param q Passed Question Database class
     */

    private void refreshQuestion(Question q) {
        final FontFitTextView Question = (FontFitTextView) findViewById(R.id.Question);
        final ToggleButton Answer1 = (ToggleButton) findViewById(R.id.AW1t);
        final ToggleButton Answer2 = (ToggleButton) findViewById(R.id.AW2t);
        final ToggleButton Answer3 = (ToggleButton) findViewById(R.id.AW3t);
        final ToggleButton Answer4 = (ToggleButton) findViewById(R.id.AW4t);
        switch (getRand()) {
            case 1:
                Question.setText(q.getQuestion());
                Answer1.setText(q.getAnsw_true());
                Answer2.setText(q.getAnsw()[1]);
                Answer3.setText(q.getAnsw()[2]);
                Answer4.setText(q.getAnsw()[0]);
                break;
            case 2:
                Question.setText(q.getQuestion());
                Answer4.setText(q.getAnsw_true());
                Answer3.setText(q.getAnsw()[1]);
                Answer2.setText(q.getAnsw()[2]);
                Answer1.setText(q.getAnsw()[0]);
                break;
            case 3:
                Question.setText(q.getQuestion());
                Answer2.setText(q.getAnsw_true());
                Answer4.setText(q.getAnsw()[1]);
                Answer1.setText(q.getAnsw()[2]);
                Answer3.setText(q.getAnsw()[0]);
                break;
        }
    }

    /**
     * Passes Questions to Result view
     *
     * @param cat  Passed Category
     * @param diff passed difficulty
     */
    private void goToResultView(int cat, int diff) {
        Intent i = new Intent(this, ResultViewActivity.class);
        i.putExtra("Cat", cat);
        i.putExtra("Diff", diff);
        startActivity(i);
        finish();
    }

    /**
     * @return returns field i
     */
    public int getI() {
        return i;
    }

    /**
     * @param i sets field i
     */

    public void setI(int i) {
        this.i = i;
    }


    public Question[] getQuestions() {
        return questions;
    }

    /**
     * setter
     *
     * @param questions Question Array
     */

    public void setQuestions(Question[] questions) {
        this.questions = questions;
    }


    /**
     * Creates OnClickListener for Mode3
     *
     * @return matching OnclickListener
     */

    private View.OnTouchListener mode3() {

        final ToggleButton Answer1 = (ToggleButton) findViewById(R.id.AW1t);
        final ToggleButton Answer2 = (ToggleButton) findViewById(R.id.AW2t);
        final ToggleButton Answer3 = (ToggleButton) findViewById(R.id.AW3t);
        final ToggleButton Answer4 = (ToggleButton) findViewById(R.id.AW4t);

        View.OnTouchListener listener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Handler handler = new Handler();
                if (Answer1.isChecked()) {

                    handler.postDelayed(new Runnable() {
                        public void run() {
                            Answer1.setChecked(false);
                            Answer2.setChecked(true);
                            refreshQuestion(getQuestions()[getI()]);
                            return;
                        }
                    }, 200);

                } else if (Answer2.isChecked()) {

                    handler.postDelayed(new Runnable() {
                        public void run() {
                            Answer2.setChecked(false);
                            Answer3.setChecked(true);
                            refreshQuestion(getQuestions()[getI()]);
                            return;
                        }
                    }, 200);

                } else if (Answer3.isChecked()) {
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            Answer3.setChecked(false);
                            Answer4.setChecked(true);
                            refreshQuestion(getQuestions()[getI()]);
                            return;
                        }
                    }, 200);
                } else if (Answer4.isChecked()) {
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            Answer4.setChecked(false);
                            Answer1.setChecked(true);
                            refreshQuestion(getQuestions()[getI()]);
                            return;
                        }
                    }, 200);
                }
                return false;
            }
        };
        return listener;
    }

    /**
     * This Method handles the back Button
     */

    @Override
    public void onBackPressed() {
        Intent i = new Intent(QuizView3Activity.this, MainActivity.class);
        if (doubleBackToExitPressedOnce) {
            startActivity(i);
            super.onBackPressed();
            finish();
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Nochmal drücken um abzubrechen", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);

    }
}

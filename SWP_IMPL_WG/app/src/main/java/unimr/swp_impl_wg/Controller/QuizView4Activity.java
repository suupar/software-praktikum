package unimr.swp_impl_wg.Controller;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.Observable;
import java.util.Observer;
import java.util.Random;

import unimr.swp_impl_wg.Helper.DebouncedOnClickListener;
import unimr.swp_impl_wg.Helper.FontFitTextView;
import unimr.swp_impl_wg.Model.Question;
import unimr.swp_impl_wg.Model.QuestionData;
import unimr.swp_impl_wg.Model.SensorData;
import unimr.swp_impl_wg.R;
import unimr.swp_impl_wg.sensor.BreathMode;
import unimr.swp_impl_wg.sensor.BreathSensorDataProcessor;


/**
 * This class Handles the Question answering sequence 4
 *
 * @author Tim Waldhans, fabio Geiger
 */

public class QuizView4Activity extends AppCompatActivity {
    /**
     * @param questions Represents Questions sequence
     * @param rand Golbal random int value
     * @param i Counter for questions to be shown
     * @param doubleBackToExitPressedOnce Boolean for pressed Button
     */
    private Question[] questions;
    private int rand;
    private int i = 0;
    boolean doubleBackToExitPressedOnce = false;

    public int getRand() {
        return rand;
    }

    public void setRand(int rand) {
        this.rand = rand;
    }


    /**
     * OnCreate function, manages Question pick with Breath movements
     *
     * @param savedInstanceState InstanceState
     * @see Bundle
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_view4);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ToggleButton Answer3 = (ToggleButton) findViewById(R.id.AW3t);
        final ToggleButton Answer4 = (ToggleButton) findViewById(R.id.AW4t);
        final int cat = getIntent().getIntExtra("Cat", 0), diff = getIntent().getIntExtra("Diff", 0);
        final SharedPreferences prefs = this.getSharedPreferences("com.quizapy.app", MODE_PRIVATE);

        Answer3.setVisibility(View.INVISIBLE);
        Answer4.setVisibility(View.INVISIBLE);

        final Button einatmen = (Button) findViewById(R.id.bIN4);
        einatmen.setOnClickListener(mode4()[0]);

        final Button ausatmen = (Button) findViewById(R.id.bOUT4);
        ausatmen.setOnClickListener(mode4()[1]);

        unimr.swp_impl_wg.Model.Question[] questions = QuestionData.pickQuestions(cat, diff, prefs);
        setQuestions(questions);
        displayQuestion(getQuestions()[getI()]);
        if (prefs.getBoolean("BT", false)) {
            SensorData.getInstance().getBp().addObserver(new Observer() {
                @Override
                public void update(Observable o, Object arg) {
                    BreathSensorDataProcessor dp = (BreathSensorDataProcessor) o;
                    if (dp.getBreathModes().contains(BreathMode.LUFT_ANHALTEN_AUSATMEN)) {
                        einatmen.callOnClick();
                        return;
                    } else if (dp.getBreathModes().contains(BreathMode.LUFT_ANHALTEN_EINATMEN)) {
                        ausatmen.callOnClick();
                        return;
                    }

                }
            });
        }

    }

    /**
     * Creates OnClickLIstener for Mode 4
     *
     * @return Matching OnClickListener
     * @see android.view.View.OnClickListener
     */

    private View.OnClickListener[] mode4() {
        final ToggleButton Answer1 = (ToggleButton) findViewById(R.id.AW1t);
        final ToggleButton Answer2 = (ToggleButton) findViewById(R.id.AW2t);
        final ToggleButton Answer3 = (ToggleButton) findViewById(R.id.AW3t);
        final ToggleButton Answer4 = (ToggleButton) findViewById(R.id.AW4t);
        final int cat = getIntent().getIntExtra("Cat", 0), diff = getIntent().getIntExtra("Diff", 0);
        final TextView currMode = (TextView) findViewById(R.id.CurrentMode);
        final SharedPreferences prefs = this.getSharedPreferences("com.quizapy.app", MODE_PRIVATE);
        currMode.setText("Binär wählen \nEinatmen: Linke Antwort \nAusatmen: Rechte Antwort");

        Answer3.setVisibility(View.INVISIBLE);
        Answer4.setVisibility(View.INVISIBLE);

        DebouncedOnClickListener listenerBIN = new DebouncedOnClickListener(200) {
            @Override
            public void onDebouncedClick(View v) {
                questionAnswered(getQuestions(), Answer1.getText().toString());
                if (!getQuestions()[prefs.getInt("QuestionsPerRound", 10) - 1].getGivenAnswer().equals("")) {
                    QuestionData.getInstance().setPickedQuestions(getQuestions());
                    goToResultView(cat, diff);
                }

            }
        };

        DebouncedOnClickListener listenerBOUT = new DebouncedOnClickListener(200) {
            @Override
            public void onDebouncedClick(View v) {
                questionAnswered(getQuestions(), Answer2.getText().toString());
                if (!getQuestions()[prefs.getInt("QuestionsPerRound", 10) - 1].getGivenAnswer().equals("")) {
                    QuestionData.getInstance().setPickedQuestions(getQuestions());
                    goToResultView(cat, diff);
                }

            }
        };

        DebouncedOnClickListener[] listeners = {listenerBIN, listenerBOUT};
        return listeners;
    }

    /**
     * Manages Questions and Updates question Array with answered questions
     *
     * @param qs QuestionArray
     * @param a  postion
     */
    private void questionAnswered(Question[] qs, String a) {
        final SharedPreferences prefs = this.getSharedPreferences("com.quizapy.app", Context.MODE_PRIVATE);
        qs[getI()].setGivenAnswer(a);
        if (getI() < prefs.getInt("QuestionsPerRound", 10) - 1) {

            setI(getI() + 1);
            displayQuestion(qs[getI()]);
        }
        setQuestions(qs);

    }


    /**
     * Displays New Question in QuizView
     *
     * @param q Question to be displayed
     */
    private void displayQuestion(Question q) {
        final FontFitTextView Question = (FontFitTextView) findViewById(R.id.Question);
        final ToggleButton Answer1 = (ToggleButton) findViewById(R.id.AW1t);
        final ToggleButton Answer2 = (ToggleButton) findViewById(R.id.AW2t);
        setRand(new Random().nextInt(3) + 1);
        switch (getRand()) {
            case 1:
                Question.setText(q.getQuestion());
                Answer1.setText(q.getAnsw_true());
                Answer2.setText(q.getAnsw()[1]);
                break;
            case 2:
                Question.setText(q.getQuestion());
                Answer2.setText(q.getAnsw_true());
                Answer1.setText(q.getAnsw()[0]);
                break;
            case 3:
                Question.setText(q.getQuestion());
                Answer1.setText(q.getAnsw_true());
                Answer2.setText(q.getAnsw()[2]);
                break;
        }


    }


    /**
     * Passes Questions to Result view
     * @param cat category number
     * @param diff difficulty
     */
    private void goToResultView(int cat, int diff) {
        Intent i = new Intent(this, ResultViewActivity.class);

        i.putExtra("Cat", cat);
        i.putExtra("Diff", diff);
        startActivity(i);
        finish();
    }

    /**
     * @return returns field i
     */
    public int getI() {
        return i;
    }

    /**
     * @param i sets field i
     */

    public void setI(int i) {
        this.i = i;
    }


    public Question[] getQuestions() {
        return questions;
    }

    /**
     * setter
     *
     * @param questions Question Array
     */

    public void setQuestions(Question[] questions) {
        this.questions = questions;
    }

    /**
     * This method handles the Back button, jumps back to Main
     */

    @Override
    public void onBackPressed() {
        Intent i = new Intent(QuizView4Activity.this, MainActivity.class);
        if (doubleBackToExitPressedOnce) {
            startActivity(i);
            super.onBackPressed();
            finish();
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Nochmal drücken um abzubrechen", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);

    }
}

package unimr.swp_impl_wg.Controller;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import unimr.swp_impl_wg.Model.PointTools;
import unimr.swp_impl_wg.R;


/**
 * @author Tim Waldhans, Fabio Geiger
 *         This class displays the Unlocked SafeActivity where the User can store Points
 */

public class SafeUnlockedActivity extends AppCompatActivity {

    /**
     * OnCreate function, declares View and sets Saved Points
     *
     * @param savedInstanceState Passed InstanceState
     * @see Bundle
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_safe_unlocked);
        final SharedPreferences prefs = this.getSharedPreferences("com.quizapy.app", Context.MODE_PRIVATE);
        final TextView playingPoints = (TextView) findViewById(R.id.textView1);
        final TextView safePoints = (TextView) findViewById(R.id.textView2);
        final Button putInSafe = (Button) findViewById(R.id.button1);
        final Button takeOutOfSafe = (Button) findViewById(R.id.button2);
        String pp = playingPoints.getText().toString() + " " + PointTools.getAllPlayingPoints(prefs);
        String sp = safePoints.getText().toString() + " " + PointTools.getPoints(1, prefs);

        playingPoints.setText(pp);
        safePoints.setText(sp);


        putInSafe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PointTools.getAllPlayingPoints(prefs) > 0) {
                    if (PointTools.getPoints(0, prefs) == PointTools.getAllPlayingPoints(prefs)) {
                        Toast.makeText(SafeUnlockedActivity.this, "Es können nur gewonnene Punkte in den Safe gelegt werden!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    PointTools.setPoints(2, PointTools.getPoints(2, prefs) - 1, prefs);
                    PointTools.setPoints(1, PointTools.getPoints(1, prefs) + 1, prefs);
                    String pp = "Spielpunkte: " + PointTools.getAllPlayingPoints(prefs);
                    String sp = "Punkte im Safe: " + PointTools.getPoints(1, prefs);
                    playingPoints.setText(pp);
                    safePoints.setText(sp);
                } else {
                    Toast.makeText(SafeUnlockedActivity.this, "Es sind keine Spielpunkte vorhanden die du in den Safe legen kannst!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        takeOutOfSafe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PointTools.getPoints(1, prefs) > 0) {
                    PointTools.setPoints(2, PointTools.getPoints(2, prefs) + 1, prefs);
                    PointTools.setPoints(1, PointTools.getPoints(1, prefs) - 1, prefs);
                    String pp = "Spielpunkte: " + PointTools.getAllPlayingPoints(prefs);
                    String sp = "Punkte im Safe: " + PointTools.getPoints(1, prefs);
                    playingPoints.setText(pp);
                    safePoints.setText(sp);
                } else {
                    Toast.makeText(SafeUnlockedActivity.this, "Es sind keine Punkte im Safe gelagert!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
